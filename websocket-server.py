
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import os


class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("profile.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  client = []


  def open(self):
    print('connection opened...')
    if self not in self.client:
        print('***')
        self.client.append(self)

    self.write_message("The server says: 'Hello'. Connection was accepted.")

  def on_message(self, message):
    self.write_message("The server says: " + message + " back at you")
    print('received:', message)

  def on_close(self):
    print('connection closed...')

settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "base/template")
)
application = tornado.web.Application([
  (r'/ws', WSHandler),
  (r'/', MainHandler),
  # (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])

if __name__ == "__main__":
  application.listen(9080)
  tornado.ioloop.IOLoop.instance().start()