# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    EDITED_BY: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################
from administration.handlers import user_heyat_request

from base.handlers import index, sign_up, login, comment, uploaded_videos, websocket
from administration.handlers.video_upload import Keyword_AoutoLoadHandler,Artist_AutoLoadHandler

url_patterns = [
    (r'/', index.IndexHandler, None, 'index'),
    (r'/user_signup', sign_up.SignupHandler, None, 'usersignup'),
    (r'/user_login', login.UserLoginHandler, None, 'userlogin'),
    (r'/user_logout', login.UserLogout, None, 'user_logout'),
    (r'/user/send_comment', comment.CommentSendHandler, None, 'usersendcomment'),
    (r'/uploaded_list', uploaded_videos.UploadedListHandler, None, 'uploaded_list'),
    (r'/ws/', websocket.EchoWebSocket, None, 'ws'),
    (r'/api/', websocket.ApiHandler, None, 'api'),
    (r'/user_signup/active/', sign_up.UserActiveHandler, None, 'actibe_user'),
    (r'/user/heyat_request/', user_heyat_request.HeyatRequestHandler, None, 'request_heyat'),
    (r'/videos/auto_load/artist',Artist_AutoLoadHandler, None, 'artists_auto_load'),
    (r'/videos/auto_load/keyword',Keyword_AoutoLoadHandler, None, 'keyword_auto_load'),
    (r'/login',login.LoginHandler, None, 'login'),
    (r'/error/404',login.ErrorpageHandler, None, '404_page'),
]