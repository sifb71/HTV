# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: this module uses Converter library to convert input videos
#                 into desired output format
#
###############################################################################

# from converter import Converter
import platform
import shutil
import os
import sys


# class VideoConverter():
def convert_video(input_path, input_file_name, ext_format, output_path, out_ext):
    input_file = input_file_name + '.' + ext_format
    input_ = os.path.join(input_path, input_file)
    output_file = input_file_name + '.' + out_ext
    output_ = os.path.join(output_path, output_file)
    if out_ext is 'mp4':
        options = {
            'format': out_ext,
            'audio': {'codec': 'aac'},
            'video': {'codec': 'h264',
                      'width': 640,
                      'height': 360
            }
        }
    elif out_ext is 'webm':
        options = {
            'format': out_ext,
            'audio': {'codec': 'vorbis'},
            'video': {'codec': 'vp8',
                      'width': 640,
                      'height': 360
            }
        }

    try:
        conv = Converter().convert(input_, output_, options)
        for timecode in conv:
            pass  # can be used to inform the user about the progress

        # converted file has been saved to 'wait_for_expert' folder
        return True

    except:
        if platform.system() is 'Linux':
            # back to queue
            return sys.exc_info()[0]

        elif platform.system() is 'Windows':
            shutil.copy(os.path.join(input_path, input_file), os.path.join(output_path, input_file))

    return True

