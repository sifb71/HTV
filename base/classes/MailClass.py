﻿# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# import mandrill
# import os
# from tornado import template
#
# class HeyatTVMail():
#     def __init__(self, from_address='sifb1371@yahoo.com', from_name='HeyatTV :)', subject='HeyatTV', tag_list= None,
#                  to= None):
#         if not to:
#             print('not')
#             to = [{'email': 'sifb1371@yahoo.com', 'name': 'HeyatTV', 'type': 'to'}]
#         if not tag_list:
#             tag_list = ['HeayTV']
#         self.from_address = from_address
#         self.from_name = from_name
#         self.html = ''
#         self.subject = subject
#         self.tag_list = tag_list
#         self.to = to
#
#     def send(self ,txt):
#         try:
#             mandrill_client = mandrill.Mandrill('PcDu7RbpROprCYxpExCw4g')
#
#             message = {
#                 'auto_html': True,
#                 'auto_text': False,
#                 'bcc_address': self.from_address,
#                 'from_email': self.from_address,
#                 'from_name': self.from_name,
#
#                 'headers': {'Reply-To': 'noreply@heyattv.ir'},
#                 'html': txt,
#
#                 'important': True,
#                 'inline_css': None,
#                 'merge': True,
#                 # 'merge_vars': [{'rcpt': 'recipient.email@example.com',
#                 # 'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
#                 'metadata': {'website': 'www.heyattv.ir'},
#                 'preserve_recipients': False,
#                 # 'recipient_metadata': [{'rcpt': 'reza.s4t4n@gmail.com',
#                 #                         'values': {'user_id': 123456}}],
#                 'return_path_domain': None,
#                 'signing_domain': None,
#
#                 'subject': self.subject,
#                 'tags': self.tag_list,
#                 'text': None,
#                 'to': self.to,
#                 'track_clicks': None,
#                 'track_opens': None,
#                 'tracking_domain': None,
#                 'url_strip_qs': None,
#                 'view_content_link': None}
#
#             result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool')
#             return result
#         except():
#             print('A mandrill error occurred: %s - %s' )
#             pass
#
#     def render_html(self, path=None, template_name=None, **kwargs):
#         av_dir = os.path.join(path, "email_forms")
#
#         if not os.path.exists(av_dir):
#             os.makedirs(av_dir)
#
#         tmp = os.path.join(av_dir, template_name)
#
#         f = open(tmp).read()
#
#         t = template.Template(f)
#
#         self.html = t.generate(**kwargs)
