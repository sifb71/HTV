# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: methods in this module aim to handle input and output of 2
#                 queues on RabbitMQ server, including "wait_for_convert"
#                 and "converted"
#
###############################################################################

import os
import shutil
from contextlib import contextmanager

import pika
from administration.handlers import upload_video
from config import Config
from base.classes import video_converter


@contextmanager
def common_handling():
    try:
        yield
    finally:
        # os exception handling
        pass


class QueueHandler():
    def __init__(self, in_q, out_ext):
        self.q_name = in_q
        # self.out_folder = out_folder
        self.out_extension = out_ext

    def push(self, message, host_name='localhost'):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host_name))
        channel = connection.channel()

        channel.queue_declare(queue=self.q_name, durable=True)  # store tasks if Rabbit crash or restart

        channel.basic_publish(exchange='',
                              routing_key=self.q_name,
                              body=message,
                              properties=pika.BasicProperties(
                                  delivery_mode=2,  # make message persistent
                                  # save the message to disk
                              ))
        print(" [x] Sent %r" % (message,))
        connection.close()

    def pop(self, host_name='localhost'):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host_name))
        channel = connection.channel()

        channel.queue_declare(queue=self.q_name, durable=True)
        print(' [*] Waiting for messages. To exit press CTRL+C')

        def callback(ch, method, properties, body):
            print(" [x] Received %r" % (body,))
            # start converting
            input_args = (body.decode("utf-8")).split(',')
            if len(input_args) is 3:
                res = False
                # preparing arguments associated with input file
                [input_path, input_file_name, ext_format] = input_args

                # preparing arguments associated with output file
                video_id = input_file_name.split('-')[0]
                heyat_folder = upload_video.get_heyat_path(video_id)
                res = video_converter.convert_video(input_path, input_file_name, ext_format,
                                                    heyat_folder, self.out_extension)

                if res is True:  # it means that new file is stored in folder heyat
                    input_file = input_file_name + '.' + ext_format
                    if self.out_extension == 'mp4':
                        # set proper tag
                        upload_video.set_status(video_id, 2)

                        # remove the original file from folder 'wait_for_convert'
                        with common_handling():
                            os.remove(os.path.join(input_path, input_file))

                    else:  # self.out_extension == 'webm'
                        # call a function to set associated tags
                        upload_video.set_status(video_id, 1)

                else:  # result is an Exception, the file is not convertable
                    # send an ack to the Q to refuse consuming again
                    # raise an error and save to not_convertable folder
                    input_file = input_file_name + '.' + ext_format
                    input_ = os.path.join(input_path, input_file)
                    with common_handling():
                        shutil.move(input_, os.path.join(Config().project_path, 'not_convertable'))

            else:
                print('wrong item from queue')

            print(" [x] Done")
            ch.basic_ack(delivery_tag=method.delivery_tag)

        channel.basic_qos(prefetch_count=1)  # it will dispatch it to the next worker that is not still busy
        channel.basic_consume(callback, queue=self.q_name)

        channel.start_consuming()





        # first version:
        # def push(q_name, message, host_name='localhost'):
        #  ------------------------ first version ----------------------------------------
        # connection = pika.BlockingConnection(pika.ConnectionParameters(host_name))
        # channel = connection.channel()
        #
        # channel.queue_declare(queue=q_name)
        #
        # channel.basic_publish(exchange='', routing_key=q_name, body=message)
        # print(' Message Sent')
        # connection.close()
        # --------------------------------------------------------------------------------

        # def pop(self, q_name, host_name='localhost'):
        #  ------------------------ first version ----------------------------------------
        # connection = pika.BlockingConnection(pika.ConnectionParameters(host_name))
        # channel = connection.channel()
        #
        # channel.queue_declare(queue=q_name)
        # print(' [*] Waiting for messages.')
        #
        # channel.basic_consume(callback, queue=q_name, no_ack=True)
        #
        # channel.start_consuming()
        # --------------------------------------------------------------------------------