# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import re


# remove HTML tags
def html_checking(input_value):
    result = re.sub("<.*?>", "", input_value)
    return result


# add a slash before every ' or "
def add_slashes(input_value):
    d = {'"': '\\"', "'": "\\'", "\0": "\\\0", "\\": "\\\\"}
    return ''.join(d.get(c, c) for c in input_value)


def remove_html_comment(input_value):
    result = re.sub("<!--.*?-->", "", input_value)
    return result


def check_input(input_value):
    result = html_checking(input_value)
    result = add_slashes(result)
    result = remove_html_comment(result)
    return result