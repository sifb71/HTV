# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class IndexHandler(WebBaseHandler):
    @tornado.web.asynchronous
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if(login_id):
           login_user = session.get('fname')
           login_user +=  " "
           login_user += session.get("lname")
           heyat_id = session.get('heyat')
        else:
             login_id = 0
             login_user = 'مهمان'
        last_heyats = Heyat.select().where(Heyat.is_active ==1 ).order_by(Heyat.inserted_date.desc()).limit(4)
        best_heyat = Heyat.select().where(Heyat.is_active ==1 ).order_by(Heyat.score.desc()).limit(4)
        last_videos = Video.select().where(Video.status == 3 ).order_by(Video.inserted_date.desc()).limit(4)
        best_videos = Video.select().where(Video.status == 3 ).order_by(Video.visit.desc()).limit(5)

        self.render('index/index.html', login_user=login_user, title='هیئت تی وی',lastheyats=last_heyats,bestheyat=best_heyat,lastvideos=last_videos,bestvideos=best_videos)
