# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################
from pycket.session import SessionManager

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from base.models.methods import *
from config import Config


class VideoDomainListHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        subject_domain_id = 1
        subject_domain = Sub_Domain.select().where(Sub_Domain.id == subject_domain_id).get()
        videos = Video.select().where( Video.fk_user_expert == None,
                                      Video.status == 1)
        self.render('video_domain_list.html', videos=videos)

    def post(self, *args, **kwargs):
        video_id = self.get_argument('id')

        session = SessionManager(self)
        expert_id = session.get('id')
        expert = User().select().where(User.id == expert_id).get()
        Video().update(fk_user_expert=expert).where(Video.id == video_id).execute()
        self.redirect(self.reverse_url('show_domain_video'))


class AddDomainForVideoHandler(WebBaseHandler):

    def get(self, *args, **kwargs):
        subject_domain = SubjectDomain_has_Video().select()\
        .group_by(SubjectDomain_has_Video.fk_video)

        array_video_id = []
        for item in subject_domain:
            array_video_id.append(item.fk_video.id)

        if array_video_id == []:
            array_video_id.append(-1)


        videos = Video.select().where( ~(Video.id << array_video_id) , Video.fk_user_expert == None, Video.status == 1)
        print(videos.count())
        domain = Subject_Domain.select().where(Subject_Domain.id!= 9)
        self.render('add_domain_video.html', videos=videos, domain=domain)

    def post(self, *args, **kwargs):
        print('post')
        video_id = self.get_argument('id')
        video = Video.select().where(Video.id == video_id).get()

        domains = self.get_argument('domains')
        domains_id = domains.split(',')

        for item in domains_id:
            domain = Subject_Domain.select().where(Subject_Domain.id == item).get()
            SubjectDomain_has_Video().create(fk_subject_domain= domain, fk_video=video)
        self.redirect(self.reverse_url('add_domain_video'))


