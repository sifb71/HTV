# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################
from pycket.session import SessionManager

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from base.models.methods import *
from pycket.session import SessionManager


class Video_List_scoreHandler(WebBaseHandler):
        def get(self, *args, **kwargs):
            ####################################################
            title = "داشبورد"
            page_name = "کارشناسی"
            page_name2 = "دادن امتیاز به ویدئو"
            s = SessionManager(self)
            login_name = s.get('fname')
            login_name += " "
            login_name += s.get("lname")
            login_user = User.select().where(User.id == int(s.get('id'))).get()
            user = User.select().where(User.id == s.get('id'))
            notifications = Notification.select().where(Notification.fk_user == user)
            new_notification = Notification.select().where(Notification.is_read == 0, Notification.fk_user == user)
            count_notification = new_notification.select().count()
            ##########################################################

            expert = User().select().where(User.id == s.get('id'))
            exprt_domain = Expert_has_Domain.select(Expert_has_Domain.fk_domain)\
                .where(Expert_has_Domain.fk_user_expert == expert)

            subject_videoes = SubjectDomain_has_Video(SubjectDomain_has_Video.fk_video).\
                select().where(SubjectDomain_has_Video.fk_subject_domain << exprt_domain )

            videos = Video().select().where(Video.id << subject_videoes , Video.fk_user_expert == expert)


            self.render('show_video_for_score_list.html', videos=videos ,notifications=new_notification, new_notification=new_notification,count_notification=count_notification,
              title=title, page_name=page_name, page_name2=page_name2,login_name=login_name,login_user=login_user)
