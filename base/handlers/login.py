# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import hashlib
from base.handlers.base import WebBaseHandler

from ..models.base_model import *
from ..classes import input_checker
from pycket.session import SessionManager
from redis import *



class UserLoginBaseHandler(WebBaseHandler):
    def __init__(self, application, request, **kwargs):
        super(UserLoginBaseHandler, self).__init__(application, request, **kwargs)
        self.result = {
            'status': False,
            'message': '',
            'errors': [],
            'data': None
        }

    def on_finish(self):
        # print('comment db close')
        super(UserLoginBaseHandler, self).on_finish()



class UserLoginHandler(UserLoginBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'
        self.render('login1.html', login_user=login_user, title='ورود', globmsg="")



    def post(self, *args, **kwargs):

        email = input_checker.check_input(self.get_argument('username'))
        passw = input_checker.check_input(self.get_argument('pass'))



        if email == '':
            self.result['errors'] = 'ایمیل را وارد کنید.'
        elif passw == '':
            self.result['errors'] = 'گذرواژه را وارد کنید.'
        m = hashlib.md5()
        m.update(passw.encode('utf-8'))
        passw = m.hexdigest()

        if email and passw:
            try:
                mem = User.select().where(User.email == email).get()
                if mem.password == passw:
                    if mem.is_active:
                        session = SessionManager(self)
                        session.set('id', mem.id)
                        session.set('fname', mem.fore_name)
                        session.set('lname', mem.sur_name)
                        session.set('email', mem.email)
                        session.set('role', mem.fk_role.id)
                        try:
                            heyat_id = Heyat.select().where(Heyat.fk_admin == mem).get().id
                            session.set('heyat' , heyat_id)

                        except:
                            print('ex')
                            session.set('heyat', None)

                        self.result['errors'] = 'ورود با موفقیت.'
                        self.result['status'] = True

                    else:
                        self.result['errors'] = "حساب شما غیر فعال می باشد!"
                else:
                    self.result['errors'] = "نام کاربری یا گذرواژه اشتباست!"
                    print('Wpsss')
            except:
                self.result['errors'] = "نام کاربری یا گذرواژه اشتباست!"
                print('Wemail')

        if not self.result['status']:
            session = SessionManager(self)
            login_id = session.get('id')
            if login_id:
                login_user = session.get('fname')
                login_user += " "
                login_user += session.get("lname")
            else:
                login_user = 'مهمان'
            self.render('login1.html', login_user=login_user, title='ورود', globmsg=self.result['errors'])

        else:
            self.redirect('/')


class UserLogout(UserLoginBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        for i in session.keys():
            session.delete(i)
        self.redirect(self.reverse_url('index'))

class LoginHandler(UserLoginBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if(login_id):
           login_user = session.get('fname')
           login_user +=  " "
           login_user += session.get("lname")
        else:
             login_user = 'مهمان'
        self.render('login1.html',title="ورود به سایت",login_user=login_user,globmsg="لطفا وارد حساب کاربری خود شوید")
    def post(self, *args, **kwargs):
        pass
class ErrorpageHandler(UserLoginBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if(login_id):
           login_user = session.get('fname')
           login_user +=  " "
           login_user += session.get("lname")
        else:
             login_user = 'مهمان'
        self.render('404.html', title="خطای سیستم", login_user =login_user)
    def post(self, *args, **kwargs):
        pass
