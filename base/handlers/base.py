#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'FoFo'


import tornado.web
# from Web.classes.auth.base import *
# from Web.db_models.table_models import *
from pycket.session import SessionMixin
from pycket.notification import NotificationMixin
from ..models.base_model import admin_db

class BaseHandler(tornado.web.RequestHandler, SessionMixin, NotificationMixin):
    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)


class WebBaseHandler(BaseHandler):
    def __init__(self, application, request, **kwargs):
        super(WebBaseHandler, self).__init__(application, request, **kwargs)
        admin_db.connect()


    def on_finish(self):
        pass
        admin_db.close()

def error_handler(self, status_code, **kwargs):
    if status_code == 404:
        self.render("404.html", title='404', login_user=u" ")
    else:
        self.render("error.html")



tornado.web.RequestHandler.write_error = error_handler
