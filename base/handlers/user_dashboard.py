# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################


import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler
from config import Config
# from expert_boot import *

from pycket.session import SessionManager
from base.models.base_model import Video, User, Notification, Heyat, Heyat_has_user


class DashboardHandler(WebBaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, *args, **kwargs):
        s = SessionManager(self)
        login_role = s.get("role")
        print(login_role)
        _url = Config().packages['ADMIN_SYSTEM']
        url = _url + "/administration/dashboard"
        self.redirect(url)


    def post(self, *args, **kwargs):
        pass
