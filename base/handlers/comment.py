# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from ..models.base_model import *
# from converter import Converter
from ..classes import input_checker


class CommentBaseHandler(WebBaseHandler):
    def __init__(self, application, request, **kwargs):
        super(CommentBaseHandler, self).__init__(application, request, **kwargs)
        self.result = {
            'status': False,
            'message': '',
            'errors': [],
            'data': None
        }


# noinspection PyBroadException
class CommentSendHandler(CommentBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        # c = Converter()
        # print(1)
        # info = c.probe('/t.mp4')
        # conv = c.convert('t.mp4', '/tmp/output.mkv', {
        #     'format': 'mkv',
        #     'audio': {
        #         'codec': 'mp3',
        #         'samplerate': 11025,
        #         'channels': 2
        #     },
        #     'video': {
        #         'codec': 'h264',
        #         'width': 720,
        #         'height': 400,
        #         'fps': 15
        #     }})
        #
        # for time_code in conv:
        #     print("Converting (%f) ...\r" % time_code)

        self.render('send_comment.html')

    def post(self, *args, **kwargs):
        print('post')
        try:
            video = Video.select().where(Video.id == 1).get()
            member = User.select().where(User.id == 1).get()
            comment = input_checker.input_checker(self.get_argument('comment'))
            if len(comment) == 0:
                self.result['errors'].append('please Enter Comment')

            else:
                Comment.create(video=video, member=member, comment=comment)
                self.result['message'] = "comment sent successfully"
                self.result['status'] = True

        except:
            self.result['errors'].append('an error occurred')

        if self.result['status'] == True:
            self.write(self.result)

        else:
            self.write(self.result)
