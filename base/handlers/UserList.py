# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from base.models.base_model import *
from base.models.methods import *
from pycket.session import SessionManager


class User_ListHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        session = SessionManager(self)
        user_id = session.get('id')
        user = User().select().where(User.id == user_id).get()
        user_role_id = session.get('role')
        heyat_id = session.get('heyat')
        user_role = Role().select().where(Role.id == user_role_id).get()
        role_level = user_role.level
        users = None

        if role_level == 1:  # if curent user's level is Kol
            print('l1')
            # select all other user
            users = User().select().where(User.id != user_id)

        elif role_level == 2:  # if curent user's level is Province
            if user.fk_address is not None:
                # select all user in my province
                address = Address().select().where(Address.province == user.fk_address.province)

                users = User().select() \
                    .where(User.fk_address << address, User.id != user_id)

            else:
                print('l2 else')
                print('user do not have address')
                self.write('user do not have address')

        else:  # if curent user's level is Heyat
            print('l3')
            heyat = Heyat().select().where(Heyat.id == heyat_id)
            heyat_member = Heyat_has_user().select(Heyat_has_user.fk_user) \
                .where(Heyat_has_user.fk_heyat == heyat)
            # select all user who are member in my Heyat
            users = User().select().where(User.id << heyat_member)

        self.render('show_users.html', users=users)


    def post(self, *args, **kwargs):
        print('post')


