# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

from tornado import gen
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import uuid
import hashlib
from base.handlers.base import WebBaseHandler
from ..classes import MailClass
from ..models.base_model import *
from ..classes import input_checker
from pycket.session import SessionManager


def create_active_link( email , act):
    link = "http://localhost:8095/user_signup/active/?email="+ email + "&validation="+str(act)
    print(link)
    return link


class SignupBaseHandler(WebBaseHandler):
    def __init__(self, application, request, **kwargs):
        super(SignupBaseHandler, self).__init__(application, request, **kwargs)
        self.result = {
            'status': False,
            'message': '',
            'errors': [],
            'data': None
        }

    def on_finish(self):
        super(SignupBaseHandler, self).on_finish()


globmsg = 0
globvar = {}
globvar['match'] = 0
globvar['exist'] = 0
globvar['message'] = 0
globvar['all'] = 0
globvar['fname'] = ""
globvar['lname'] = ""
globvar['email'] = ""
class SignupHandler(SignupBaseHandler):
    @gen.coroutine
    def get(self, *args, **kwargs):
        global globmsg
        if not self.current_user:
            session = SessionManager(self)
            login_id = session.get('id')
            if login_id:
                login_user = session.get('fname')
                login_user += " "
                login_user += session.get("lname")
            else:
                login_user = 'مهمان'
            self.render('signup1.html', login_user=login_user, title='عضویت', globmsg=globmsg, globvar=globvar)
            globvar['match'] = 0
            globvar['exist'] = 0
            globvar['message'] = 0
            globvar['all'] = 0
            globvar['fname'] = ""
            globvar['lname'] = ""
            globvar['email'] = ""
            globmsg = 0

        else:
            print('go to dashboard.')
            # go to dashboard :: self.redirect(self.reverse_url("dashboard"))

    @gen.coroutine
    def post(self, *args, **kwargs):
        print('post')
        global globvar
        global globmsg
        if self.get_current_user() is not None:
            self.redirect(self.reverse_url('index'))
            return

        globvar['fname'] = fname = input_checker.check_input(self.get_argument('fname'))
        globvar['lname'] = lname = input_checker.check_input(self.get_argument('lname'))
        globvar['email'] = email = input_checker.check_input(self.get_argument('email'))
        passw = input_checker.check_input(self.get_argument('pass'))
        pass2 = input_checker.check_input(self.get_argument('pass2'))
        flag = False

        if fname and lname and email and pass2 and passw:
            if pass2 != passw:
                print("passwords doesn't match")

                globvar['match'] = 1
                self.result['errors'].append("گذرواژه ها یکسان نیستند!")
            else:

                try:
                    mem = User.select().where(User.email == email).get()
                    if mem:
                        print("exist")
                        globvar['exist'] = 1
                        self.result['errors'].append('این ایمیل قبلا ثبت شده!')
                except:
                    flag = True

                if (flag):
                    try:
                        random_link = uuid.uuid4()
                        _role = Role.select().where(Role.id == 10).get()
                        # hashing password ---------------------------
                        m = hashlib.md5()
                        m.update(passw.encode('utf-8'))
                        passw = m.hexdigest()

                        User.create(fore_name=fname, sur_name=lname, password=passw, email=email, fk_role=_role,
                                    is_deleted=0)
                        mem = User.select().where(User.email == email).get()
                        link = create_active_link(email, random_link)
                        print("user added")
                        to = [{'email': email, 'name': fname + " " + lname, 'type': 'to'}]
                        html1 = '<html dir="ltr"><head><meta charset="utf-8"><style>/*september 15,2015 13:02 - mohammad hefazi. All rights reserved.heyat-TV*/body{font-size: 15px;color: #333;background-color: #fff;margin: 20px;}span{color: rgb(51, 53, 58); font-family: ''Segoe UI Local'', ''Segoe WP'', ''Segoe UI Web''; line-height: 20.0005px; white-space: pre-wrap; }</style></head><body class="cke_editable cke_editable_themed cke_contents_ltr" spellcheck="false" style="direction: rtl;"><p><strong><span>کاربر گرامی،'+' '+ mem.fore_name +' '+ mem.sur_name+ ' ' +'</span></strong><br><br><span>به پایگاه بزرگ <strong>هیئت تی&zwnj;وی</strong> خوش آمدید.</span><br><br><span>جهت فعالسازی حساب کاربری خود، بر روی لینک زیر کلیک فرمائید.</span><br><br><a href="'
                        html2 = '</a><br><br><span>با سپاس،</span><br><br><span>پشتیبانی هیئت تی وی</span><strike></strike></p></body></html>'
                        html = html1 + link + '">' + link + html2
                        MailClass.HeyatTVMail('admin@HeayTv.ir', 'هیئت تی وی', 'کد فعال سازی', '', to).send(html)
                        User_ActivationLink.create(activation_link=random_link, fk_user=mem)
                        globvar['message'] = 1
                        self.result['errors'].append('حساب کاربری شما ایجاد و لینک فعال سازی به ایمیل شما ارسال شد.')
                    except Exception as e:
                        globvar['message'] = 1
                        try:
                            User.delete().where(User.id == mem.id).execute()
                            self.result['errors'].append('حساب کاربری شما ایجاد و لینک فعال سازی به ایمیل شما ارسال شد.')
                            print(e)

                        except Exception as e:
                            self.result['errors'].append('متاسفانه در حین اجرای عملیات خطایی رخ داده است.لطفا مجددا تلاش نمایید')
                            print(e)
        else:
            globvar['all'] = 1
            self.result['errors'].append('تمامی فیلد ها را پر کنید')
            # self.render('index/message.html', UN=self.result['errors'])
            # print(self.result['errors'])
        globmsg = self.result['errors'][0]
        self.redirect('/user_signup')




class UserActiveHandler(SignupBaseHandler):

    def get(self, *args, **kwargs):
        print('get')
        email =self.get_argument('email')
        valid = self.get_argument('validation')

        try:
            mem = User().select().where(User.email == email).get()
            active = User_ActivationLink().select().where(User_ActivationLink.fk_user == mem).get().activation_link

            if(active == valid):

                User.update(is_active = 1).where(User.id == mem.id).execute()
                session = SessionManager(self)
                session.set('id', mem.id)
                session.set('fname', mem.fore_name)
                session.set('lname', mem.sur_name)
                session.set('email', mem.email)
                session.set('role', mem.fk_role.id)
                session.set('heyat', None)

                self.redirect(self.reverse_url('index'))


            else:
                print('else')
                pass
        except:
            print('ex')
            pass

        pass

    def post(self, *args, **kwargs):
        pass






