# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import datetime
from multiprocessing import Lock
from base.handlers.base import WebBaseHandler

from ..models.base_model import Video, User, Role
from pycket.session import SessionManager


class UploadedListHandler(WebBaseHandler):
    def data_received(self, chunk):
        print("data_received")

    def get(self, *args, **kwargs):
        print("start get!")
        s = SessionManager(self)
        role = s.get('role')

        experts = User.select().where(User.fk_role == 3)
        videos = Video.select().where(Video.fk_user_expert==None)
        if role == 1:
            flag = 1
        else:
            flag = 0
        self.render('uploaded_list.html', videos=videos, experts=experts, flag=flag)
        print("done")

    def post(self, *args, **kwargs):
        print("start post")

        session = SessionManager(self)
        expert_id = session.get('id')

        list1 = []
        list2 = []
        if expert_id == 1:
            expert_id = int(self.get_argument('chose_expert'))

        expert = User.select().where(User.id == expert_id)
        counter = 0
        tim = datetime.datetime.now()
        for video in Video.select().where(Video.fk_user_expert==None):
            name = 'select_' + str(video.id)
            list1.insert(counter, int(self.get_argument(name)))
            list2.insert(counter, video.id)
        for x in list1:
            mutex = Lock()
            with mutex:
                if x:
                    exp = User.select().where(User.id == expert_id).get()
                    try:
                          expert = Video.select().where(Video.fk_user_expert == exp).get()
                          error ="این فیلم قبلا توسط یک کارشناس دیگر انتخاب شده است"
                          self.render('index/message.html', UN=error)
                    except:
                       try:
                              Video.update(fk_user_expert=expert, date_expert=tim).where(
                              Video.id == list2[counter]).execute()
                              counter += 1
                       except:
                           error = "خطای ناشناس"
                           self.render('index/message.html', UN=error)



        self.redirect(self.reverse_url("uploaded_list"))
        print("done")
