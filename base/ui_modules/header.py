#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule


class MyHeader(UIModule):
    def render(self, login_user):

        return self.render_string("modules/header.html", login_user=login_user)