###############################################################################
#
#    AUTHORS: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
#
###############################################################################

from base.ui_modules.sidebar import *
from base.ui_modules.header import *
from base.ui_modules.top_menu import *
from base.ui_modules.MainSlider import *
from base.ui_modules.most_view_videos import *
from base.ui_modules.last_videos import *
from base.ui_modules.group_tabs import *
from base.ui_modules.main_page_center_boxes import *
from base.ui_modules.footer import *