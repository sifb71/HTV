#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule

class main_page_center_boxes(UIModule):
    def render(self):
        return self.render_string("modules/main_page_center_boxes.html")