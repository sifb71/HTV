#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule


class footer(UIModule):
    def render(self):
        return self.render_string("modules/footer.html")

