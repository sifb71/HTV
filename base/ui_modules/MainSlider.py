#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule

class MySlider(UIModule):
    def render(self):
        return self.render_string("modules/slider.html")


