#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule

class group_tabs(UIModule):
    def render(self):
        return self.render_string("modules/group_tabs.html")