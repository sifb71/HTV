#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tornado.web import UIModule

__author__ = 'ReS4'

# from base.handlers.base import *
# from base.classes.auth.login import LoginMethod


class Sidebar(UIModule):
    def render(self, role, activation=None):
        if role == "PRINCIPAL":
            ls = [
                # {
                #     "url": self.handler.reverse_url("dashboard"),
                #     "activation": "DASHBOARD",
                #     "name": "داشبورد",
                #     "child": []
                # }

            ]

            return self.render_string("modules/sidebar/sidebar.html", _tree=ls, activation=activation)

    def embedded_javascript(self):
        return '''$("li[data-id="+$("a.active").attr("data-id")+"]").addClass("active");'''