#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule

class MyTopmenu(UIModule):
    def render(self):
        return self.render_string("modules/top_menu.html")