#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Omid'

from tornado.web import UIModule

class most_view_videos(UIModule):
    def render(self):
        return self.render_string("modules/most_view_videos.html")