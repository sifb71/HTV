# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: mohammad_hefazi, iman_feyzbakhsh
#    EDITOR: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: this module aims to define all ORM funcs for database models
#
#
###############################################################################
from collections import defaultdict

import datetime
from peewee import *
from base.global_var import GlobalVar

gv = GlobalVar()
admin_db = MySQLDatabase(gv.mysql_admin_db_name, host=gv.mysql_admin_host, user=gv.mysql_admin_user,
                         passwd=gv.mysql_admin_pass)


class PeeweeBaseModel(Model):
    class Meta:
        database = admin_db


# admin_models
class Action(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=60)
    icon = CharField(max_length=50)


class Sub_Action(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=60)
    fk_action = ForeignKeyField(Action, related_name='sub_Action_Action')
    url = CharField(max_length=100)


class Role(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=30)
    level = IntegerField(1, default=3)  #1->kol  2->ostan n3->heyat


class Role_Action(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_role = ForeignKeyField(Role, related_name='actions')
    fk_sub_action = ForeignKeyField(Sub_Action, related_name='sub_action')


class Address(PeeweeBaseModel):
    id = PrimaryKeyField()
    province = CharField(max_length=50)
    city = CharField(max_length=50)
    village = CharField(max_length=50, null=True)
    address = TextField()
    is_deleted = IntegerField(1, default=False)


class User(PeeweeBaseModel):
    id = PrimaryKeyField()
    fore_name = CharField(max_length=30)
    sur_name = CharField(max_length=30)
    password = CharField(max_length=50)
    email = CharField(max_length=50)
    pic = CharField(max_length=50, null=True, default="user.png")
    is_active = IntegerField(1)
    fk_role = ForeignKeyField(Role, related_name='users', null=True)
    fk_address = ForeignKeyField(Address, related_name='user_address')
    inserted_date = DateTimeField(default=datetime.datetime.now())
    is_deleted = IntegerField(1, default=False)
    phone = CharField(max_length=11,default=" ")
    score = FloatField(default=0)
    about_me = TextField(null=True)


class User_ActivationLink(PeeweeBaseModel):
    id = PrimaryKeyField()
    # role = PrimaryKeyField()
    activation_link = CharField(max_length=36)
    inserted_date = DateTimeField(default=datetime.datetime.now())
    fk_user = ForeignKeyField(User, related_name='activation_link')


class Heyat(PeeweeBaseModel):
    id = PrimaryKeyField()
    logo = CharField(max_length=255, default="heyat.jpg")
    name = CharField(max_length=45)
    inserted_date = DateTimeField(datetime.datetime.now())
    register_date = CharField(max_length=11, null=True)
    org_register_date = CharField(max_length=11, null=True)
    mashaar_register_date = CharField(max_length=11, null=True)
    statute = CharField(max_length=255, null=True)
    score = FloatField(default=0)
    fk_address = ForeignKeyField(Address, related_name='Heyat_address')  # Heyat Address
    fk_admin = ForeignKeyField(User, related_name='Heyat_admin')  # Heyat Admin
    is_deleted = IntegerField(1, default=False)
    is_active = IntegerField(1, default=False)  # false showing us this is a new Heyat
    level = IntegerField(11, default=1)

    # addresses_id = IntegerField()
    # user_id = IntegerField()
    # event_id = IntegerField()


class Notification(PeeweeBaseModel):
    id = PrimaryKeyField()
    text = TextField()
    fk_sender = ForeignKeyField(User, related_name='notification_sender')
    fk_receiver = ForeignKeyField(User, related_name='notification_receiver')
    inserted_date = DateTimeField(default=datetime.datetime.now())
    is_read = IntegerField(1, default=0)



class Subject_Domain(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=60)


class Sub_Domain(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=60)
    ratio = IntegerField(default=1)
    fk_subject_domain = ForeignKeyField(Subject_Domain, related_name='Sub_Domain')


class Video(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=45)
    type = CharField(max_length=1)  #o->one  s->seryali
    url = CharField(max_length=255, null=True)
    headline = CharField(max_length=255)
    headline_main = CharField(max_length=255)
    format = CharField(max_length=1, default='v')  # v for video file and m for MusicFile
    comment = CharField(max_length=255)
    visit = IntegerField(default=0)
    score = FloatField(default=0)
    fk_address = ForeignKeyField(Address, related_name='video_address')  # what the hell is this?
    fk_heyat = ForeignKeyField(Heyat, related_name='videos')
    fk_uploader = ForeignKeyField(User, related_name='uploaded_video')
    fk_user_expert = ForeignKeyField(User, related_name='judged_videos', null=True)
    inserted_date = DateTimeField(datetime.datetime.now())
    date_expert = DateField(null=True)
    status = IntegerField(1, default=-1)  #0 for convert queue - 1 for converted and -1 show video dont upload yet
    pic = CharField(max_length=100, null=True)
    date = CharField(max_length=11)
    ext_format = CharField(max_length=10,null=True)

# class Url(PeeweeBaseModel):
#     id = PrimaryKeyField()
#     url = CharField(max_length=255)
#     fk_video = ForeignKeyField(Video, related_name='urls')


class Keyword(PeeweeBaseModel):
    id = PrimaryKeyField()
    name = CharField(max_length=30)


# till here exist in .sql file
class Video_Keyword(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_keyword = ForeignKeyField(Keyword, related_name='video')
    fk_video = ForeignKeyField(Video, related_name='keyword')


class Comment(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_video = ForeignKeyField(Video, related_name='video_comments')
    fk_user = ForeignKeyField(User, related_name='comment_writer')
    text = TextField()
    inserted_date = DateTimeField(default=datetime.datetime.now)
    status = IntegerField(1, default=0)


class Reply(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_comment = ForeignKeyField(Comment, related_name='comment_')
    fk_reply = ForeignKeyField(Comment, related_name='reply')
    status = CharField(1, default='s')  # s-> submitted  , a-> accepted


class Heyat_has_user(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_user = ForeignKeyField(User, related_name="heyat_has_member")
    fk_heyat = ForeignKeyField(Heyat, related_name="heat_id")
    is_accept = IntegerField(1, default=0) #1 for accepted member  0 for Dar Hale Barresi!! -1 for deleted requests
    inserted_date = DateTimeField(datetime.datetime.now())
    flag = IntegerField(1,default=0)


class Expert_has_Domain(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_user_expert = ForeignKeyField(User, related_name='user_expert_expert')
    fk_domain = ForeignKeyField(Subject_Domain, related_name='subject_domain_expert')


class SubjectDomain_has_Video(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_video = ForeignKeyField(Video, related_name='video_has_subjectDomain')
    fk_subject_domain = ForeignKeyField(Subject_Domain, related_name='subject_domain_video')
    fk_expert = ForeignKeyField(User , null=True, related_name='video_expert')
    status = IntegerField(1, default=0)

class Artist_has_Video(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_user = ForeignKeyField(User, related_name='user_artist')
    fk_video = ForeignKeyField(Video , related_name='video_uploaded')

class Score_has_Video(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_video = ForeignKeyField(Video , related_name='video_score')
    fk_sub_domain = ForeignKeyField(Sub_Domain , related_name='subject_domain_score')
    score = FloatField(default=0)

class Heyat_Has_Follower(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_heyat = ForeignKeyField(Heyat , related_name='heyat_follower')
    fk_user = ForeignKeyField(User , related_name='user_follower')

class Video_Has_Comment(PeeweeBaseModel):
    id = PrimaryKeyField()
    fk_video = ForeignKeyField(Video, related_name='video_comment')
    fk_comment = ForeignKeyField(Comment, related_name='comment_video')
    inserted_date = DateTimeField(datetime.datetime.now())
    status = IntegerField(1,default=0)

class Video_Like(PeeweeBaseModel):
     id = PrimaryKeyField()
     fk_video = ForeignKeyField(Video, related_name='video_like')
     fk_usre = ForeignKeyField(User,related_name='user_like')
     inserted_date = DateTimeField(datetime.datetime.now())



#
# if __name__ == "__main__":
#     admin_db.create_tables([
#         Action,
#  	Sub_Action,
# 	Role,
# 	Role_Action,
# 	Address,
# 	User,
# 	User_ActivationLink,
#  	Heyat,
#  	Notification,
#  	Subject_Domain,
#  	Sub_Domain,
#  	Video,
#  	Keyword,
#  	Video_Keyword,
#  	Comment,
#  	Reply,
#  	Heyat_has_user,
#  	Expert_has_Domain,
#  	SubjectDomain_has_Video,
#  	Artist_has_Video,
#  	Score_has_Video,
#  	Heyat_Has_Follower,
#  	Video_Has_Comment,
#  	Video_Like
#         ]
#     )
