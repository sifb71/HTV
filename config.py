
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: describe the general purpose of the module
#                as concisely as possible
#
#
###############################################################################

import os
from redis import *


class Config:
    def __init__(self):
        self.applications_root = os.path.join(os.path.dirname(__file__), "")

        self.project_path = os.path.dirname(__file__)
        if not os.path.exists(os.path.join(self.project_path, "static", "upload", "pic")):
            os.makedirs(os.path.join(self.project_path, "static", "upload", "pic"))
        self.upload_pic = os.path.join(self.project_path, "static", "upload", "pic")

        if not os.path.exists(os.path.join(self.project_path, "static", "upload", "video")):
            os.makedirs(os.path.join(self.project_path, "static", "upload", "video"))
        self.upload_video = os.path.join(self.project_path, "static", "upload", "video")

        if not os.path.exists(os.path.join(self.project_path, "wait_for_convert")):
            os.mkdir(os.path.join(self.project_path, "wait_for_convert"))
        self.wait_for_convert = os.path.join(self.project_path, "wait_for_convert")

        if not os.path.exists(os.path.join(self.project_path, "not_convertable")):
            os.mkdir(os.path.join(self.project_path, "not_convertable"))
        self.not_convertable = os.path.join(self.project_path, "not_convertable")

        self.videos = os.path.join(os.path.dirname(__file__), "upload")

        self.domain = '.localhost'
        self.base = {
            'port': 8095,
            'server_ip': '127.0.0.1',
            'redis': {
                'host': 'localhost',
                'port': 6379,
                'user': 'root',
                'password': '',
                'db_sessions': 12,
                'db_notifications': 13,
            },
            'template_address': os.path.join(os.path.dirname(__file__), "base/template"),
            'cookie_secret': "61oETz3455545gEmGeJsaT_d3f0sfgJFuf0sg2adfnp2XdTP1o/Vo=",
            # 'login_url': 'http://127.0.0.1:8095/user_login',
            # 'logout_url': 'http://127.0.0.1:8095/user_logout',
            'login_url': 'http://{0}:{1}/login'.format('server_ip','port'),
            'logout_url': 'http://{0}:{1}/logout'.format('server_ip','port'),
        }
        self.admin = {
            'port': 8096,
            'template_address': os.path.join(os.path.dirname(__file__), "administration/template"),

        }
        self.heyat = {
            'port': 8097,
            'template_address': os.path.join(os.path.dirname(__file__), "heyat/template"),

        }
        self.expert = {
            'port': 8098,
            'template_address': os.path.join(os.path.dirname(__file__), "expert/template"),
        }
        self.packages = {
            'PUBLIC_SYSTEM': 'http://{0}:{1}'.format(self.base['server_ip'], self.base['port']),
            'HEYAT_SYSTEM': 'http://{0}:{1}'.format(self.base['server_ip'], self.heyat['port']),
            'ADMIN_SYSTEM': 'http://{0}:{1}'.format(self.base['server_ip'], self.admin['port']),
            'EXPERT_SYSTEM': 'http://{0}:{1}'.format(self.base['server_ip'], self.expert['port']),
            # 'http://{0}:8095'.format(self.base['server_ip']),
            # 'http://{1}:8096'.format(self.base['server_ip']),
            # # 'HEYAT_SYSTEM': 'http://127.0.0.1:8097',
            # 'ADMIN_SYSTEM': 'http://'.format(self.base['server_ip']),
            # 'EXPERT_SYSTEM': 'http://127.0.0.1:8098',
        }

    def return_public_system_url(self):
        return self.packages['PUBLIC_SYSTEM']

if __name__ == "__main__":
    print(Config().return_public_system_url())