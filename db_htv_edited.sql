-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2015 at 10:40 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_htv`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id`, `name`) VALUES
(1, 'upload'),
(2, 'accept');
(3, 'level1'),
(4, 'level2'),
(5, 'level3'),
(6, 'comment'),
(7, 'like'),
(8, 'download'),
(9, 'showindex'),
(10, 'showchannel'),
(11, 'showdashboard'),
(12, 'deletechannel'),
(13, 'member'),
(14, 'festival'),
(15, 'expert'),
(16, 'portaladminlevel1'),
(17, 'portaladminlevel2'),
(18, 'portaladminlevel3');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'superadmin'),
(2, 'heyat_admin1');
(3, 'heyat_admin2'),
(4, 'heyat_admin3'),
(5, 'publicuser'),
(6, 'heyatmember'),
(7, 'mutablemember'),
(8, 'operator'),
(9, 'expert'),


-- --------------------------------------------------------

--
-- Table structure for table `role_action`
--

CREATE TABLE IF NOT EXISTS `role_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_role_id` int(11) NOT NULL,
  `fk_action_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_action_fk_action_id` (`fk_action_id`),
  KEY `role_action_fk_role_id` (`fk_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role_action`
--

INSERT INTO `role_action` (`id`, `fk_role_id`, `fk_action_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL,
  `province` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `village` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_persian_ci NOT NULL,
  `is_deleted` int(11) DEFAULT NULL
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `province`, `city`, `village`, `address`, `is_deleted`) VALUES
(1, 'a', 'a', 'a', 'aaa', 0),
(2, 'a', 'a', 'a', 'aaa', 0),
(3, 'a', 'a', 'a', 'aaa', 0),
(4, 'a', 'a', 'a', 'aaa', 0),
(5, 'a', 'a', '', 'a', 0),
(6, 'a', 'a', 'a', 'a', 0),
(7, 'a', 'a', NULL, 'a', 0);

-- --------------------------------------------------------
--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fore_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `sur_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `pic` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `is_active` tinyint(11) DEFAULT '0',
  `fk_role_id` int(11) NOT NULL,
  `register_date` timestamp(1) NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_fk_role_id` (`fk_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fore_name`, `sur_name`, `password`, `email`, `pic`, `is_active`, `fk_role_id`, `register_date`, `is_deleted`) VALUES
(1, 'mohammad', 'hf', '1234', 'smohe', 'url', 1, 1, '2015-08-01 19:30:00.0', 0),
(2, 'iman', 'iman', '1234', 'iman', 'url', 1, 1, '2015-08-01 19:30:00.0', 0);

-- --------------------------------------------------------
--
-- Table structure for table `user_activationlink`
--

CREATE TABLE IF NOT EXISTS `user_activationlink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activation_link` varchar(36) NOT NULL,
  `sent_date` timestamp NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_activationlink_fk_user_id` (`fk_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `heyat`
--

CREATE TABLE IF NOT EXISTS `heyat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(45) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `request_date` timestamp NOT NULL,
  `est_date` timestamp NOT NULL,
  `register_date` date NOT NULL,
  `org_register_date` date NOT NULL,
  `mashaar_register_date` date NOT NULL,
  `statute` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `score` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `heyat`
--

INSERT INTO `heyat` (`id`, `logo`, `name`, `est_date`, `register_date`, `org_register_date`, `mashaar_register_date`, `statute`, `score`) VALUES
(1, 'url', '���?��', '2015-08-01 20:44:24', '2015-08-03', '2015-08-04', '2015-08-05', '��?���?� ������� �� ��� ���? ��?� ���� �� �����ϐ�� с ����? ��� �� �� �Ș �?�� ��� ���� �� ����?� �?����.', 0),
(2, 'logo', '�??� ���', '2015-08-04 04:31:17', '0000-00-00', '0000-00-00', '0000-00-00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(45) NOT NULL,
  `text` varchar(100) NOT NULL,
  `is_read` tinyint(1) DEFAULT '0',
  `fk_user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `notification_fk_user_id` (`fk_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- Table structure for table `url`
--

CREATE TABLE IF NOT EXISTS `url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `fk_video_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `url_fk_video_id` (`fk_video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `url`
--

INSERT INTO `url` (`id`, `url`, `fk_video_id`) VALUES
(1, 'videos\\amir-tataloo.mp4', 1),
(2, 'videos\\amir-tataloo.ogv', 2),
(3, 'videos\\amir-tataloo.webm', 3);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` tinyint(1) DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `headline` varchar(255) NOT NULL,
  `headline_main` varchar(255) NOT NULL,
  `format` varchar(1) NOT NULL DEFAULT 'v',
  `comment` varchar(255) NOT NULL,
  `like` int(11) NOT NULL DEFAULT '0',
  `visit` int(11) NOT NULL DEFAULT '0',
  `score` float NOT NULL DEFAULT '0',
  `addresses_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `fk_heyat_id` int(11) NOT NULL,
  `fk_uploader_id` int(11) NOT NULL,
  `fk_user_expert_id` int(11) NOT NULL,
  `is_accepted` tinyint(1) DEFAULT '-1',
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_expert` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `video_fk_uploader_id` (`fk_uploader_id`),
  KEY `video_fk_heyat_id` (`fk_heyat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `type`, `url`, `headline`, `headline_main`, `comment`, `like`, `visit`, `score`, `addresses_id`, `subject_id`, `fk_heyat_id`, `fk_uploader_id`, `fk_user_expert_id`, `is_accepted`, `date`, `date_expert`) VALUES
(1, '��� ���� �� ��', 0, 'url', '�� �?�� ��� ���� �� �� \r\n��??��� ����� ��', '�?�� ���? ��� ���� �� ��\r\n��??��� ����� ��', '���?��� \r\n��??��� ����� ��', 0, 0, 65, 1, 1, 1, 1, 1, 1, '2015-08-01 20:47:27', '2015-08-04'),
(2, '���� ��', 0, 'url', '�� �?��qqqqqqqqqqqqqqqqq', '�?�� ���?qqqqqqq', '���?���', 0, 0, 0, 0, 0, 1, 2, 1, 0, '2015-08-01 20:53:40', '2015-08-04'),
(3, 'qs', 0, 'qs', 'qs', 'qs', 'qs', 6, 6, 12, 0, 0, 1, 1, 0, -1, '2015-08-03 11:46:37', '2015-08-04'),
(4, '��?� ���', 0, '', '', '', '', 0, 0, 0, 0, 0, 2, 1, 1, -1, '2015-08-04 04:36:30', '2015-08-04');

--
-- Constraints for dumped tables
--
-- Iman:
-- --------------------------------------------------------
--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8_persian_ci NOT NULL,
  `insert_date` datetime NOT NULL,
  `publish_date` datetime NOT NULL,
  `status` varchar(1) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `video_id`, `user_id`, `comment`, `date`, `status`) VALUES
(1, 1, 1, 'به نام خدا', '2015-08-01 23:14:20', 's'),
(2, 1, 1, 'hkjh', '2015-08-02 05:45:11', 's');

-- --------------------------------------------------------
--
-- Table structure for table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
`id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `reply_id` int(11) NOT NULL,
  `status` varchar(1) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`), 
  ADD KEY `reply_reply_id` (`reply_id`), ADD KEY `reply_comment_id` (`comment_id`);

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

CREATE TABLE IF NOT EXISTS `keyword` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `keyword`
--

INSERT INTO `keyword` (`id`, `name`) VALUES
(1, 'iman'),
(2, 'salam'),
(15, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `video_keyword`
--

CREATE TABLE IF NOT EXISTS `video_keyword` (
  `id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`id`), 
  ADD KEY `video_keyword_keyword_id` (`keyword_id`), 
  ADD KEY `video_keyword_video_id` (`video_id`);
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------
--
-- Table structure for table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
  `id` int(11) NOT NULL,
  `score` float DEFAULT NULL,
  `Biography` varchar(255) DEFAULT NULL,
  `Introduction` varchar(255) DEFAULT NULL,
  `heyat_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `artist_has_artist`
--

CREATE TABLE IF NOT EXISTS `artist_has_artist` (
  `Artist_id_follower` int(11) NOT NULL,
  `Artist_id_following` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE IF NOT EXISTS `authorities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `pic` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL,
  `heyat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `share`
--

CREATE TABLE IF NOT EXISTS `share` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL,
  `subject` varchar(45) DEFAULT NULL COMMENT 'مد?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `suggestion`
--

CREATE TABLE IF NOT EXISTS `suggestion` (
  `id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `phone`
--

CREATE TABLE IF NOT EXISTS `phone` (
  `id` int(11) NOT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `Addresses_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `heyat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newspaper`
--

CREATE TABLE IF NOT EXISTS `newspaper` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `occasion`
--

CREATE TABLE IF NOT EXISTS `occasion` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `Authorities_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `like`
--

CREATE TABLE IF NOT EXISTS `like` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `magazine`
--

CREATE TABLE IF NOT EXISTS `magazine` (
  `id` int(11) NOT NULL,
  `heyat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `educate`
--

CREATE TABLE IF NOT EXISTS `educate` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fallow_artist`
--

CREATE TABLE IF NOT EXISTS `fallow_artist` (
  `id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fallow_heyat`
--

CREATE TABLE IF NOT EXISTS `fallow_heyat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `heyat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `festival`
--

CREATE TABLE IF NOT EXISTS `festival` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_has_heyat`
--

CREATE TABLE IF NOT EXISTS `gallery_has_heyat` (
  `gallery_id` int(11) NOT NULL,
  `heyat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `heyat_has_suggestion`
--

CREATE TABLE IF NOT EXISTS `heyat_has_suggestion` (
  `heyat_id` int(11) NOT NULL,
  `suggestion_idsuggestion` int(11) NOT NULL,
  `suggestion_Video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Constraints for table `role_action`
--
ALTER TABLE `role_action`
  ADD CONSTRAINT `role_action_ibfk_1` FOREIGN KEY (`fk_role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `role_action_ibfk_2` FOREIGN KEY (`fk_action_id`) REFERENCES `action` (`id`);

-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------
-- --------------------------------------------------------
--
-- Constraints for table `video_keyword`
--
ALTER TABLE `video_keyword`
ADD CONSTRAINT `video_keyword_ibfk_1` FOREIGN KEY (`video_keyword_id`) REFERENCES `video_keyword` (`id`),
ADD CONSTRAINT `video_keyword_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `reply`
--
ALTER TABLE `reply`
ADD CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`),
ADD CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`reply_id`) REFERENCES `reply` (`id`);


--
-- AUTO_INCREMENT for table `video_keyword`
--
ALTER TABLE `video_keyword`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keyword`
--
ALTER TABLE `keyword`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- --------------------------------------------------------
--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_Artist_heyat1_idx` (`heyat_id`), ADD KEY `fk_Artist_video1_idx` (`video_id`);

--
-- Indexes for table `artist_has_artist`
--
ALTER TABLE `artist_has_artist`
 ADD PRIMARY KEY (`Artist_id_follower`,`Artist_id_following`), ADD KEY `fk_Artist_has_Artist_Artist2_idx` (`Artist_id_following`), ADD KEY `fk_Artist_has_Artist_Artist1_idx` (`Artist_id_follower`);

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`id`), ADD KEY `comment_user_id` (`user_id`), ADD KEY `comment_video_id` (`video_id`);

-- Indexes for table `educate`
--
ALTER TABLE `educate`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fallow_artist`
--
ALTER TABLE `fallow_artist`
 ADD PRIMARY KEY (`id`,`Artist_id`), ADD KEY `fk_fallow_artist_Artist1_idx` (`Artist_id`), ADD KEY `fk_fallow_artist_user1_idx` (`user_id`);

--
-- Indexes for table `fallow_heyat`
--
ALTER TABLE `fallow_heyat`
 ADD PRIMARY KEY (`id`,`heyat_id`), ADD KEY `fk_fallow_user1_idx` (`user_id`), ADD KEY `fk_fallow_heyat_heyat1_idx` (`heyat_id`);

--
-- Indexes for table `festival`
--
ALTER TABLE `festival`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_has_heyat`
--
ALTER TABLE `gallery_has_heyat`
 ADD PRIMARY KEY (`gallery_id`,`heyat_id`), ADD KEY `fk_gallery_has_heyat_heyat1_idx` (`heyat_id`), ADD KEY `fk_gallery_has_heyat_gallery1_idx` (`gallery_id`);

--
-- Indexes for table `heyat_has_suggestion`
--
ALTER TABLE `heyat_has_suggestion`
 ADD PRIMARY KEY (`heyat_id`,`suggestion_idsuggestion`,`suggestion_Video_id`), ADD KEY `fk_heyat_has_suggestion_suggestion1_idx` (`suggestion_idsuggestion`,`suggestion_Video_id`), ADD KEY `fk_heyat_has_suggestion_heyat1_idx` (`heyat_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_job_Authorities1_idx` (`Authorities_id`);

--
-- Indexes for table `like`
--
ALTER TABLE `like`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magazine`
--
ALTER TABLE `magazine`
 ADD PRIMARY KEY (`id`,`heyat_id`), ADD KEY `fk_magazine_heyat1_idx` (`heyat_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`,`heyat_id`), ADD KEY `fk_News_heyat1_idx` (`heyat_id`);

--
-- Indexes for table `newspaper`
--
ALTER TABLE `newspaper`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occasion`
--
ALTER TABLE `occasion`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone`
--
ALTER TABLE `phone`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_Phone_Addresses1_idx` (`Addresses_id`);

--
-- Indexes for table `suggestion`
--
ALTER TABLE `suggestion`
 ADD PRIMARY KEY (`idsuggestion`,`Video_id`), ADD KEY `fk_suggestion_Video1_idx` (`Video_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`,`heyat_id`), ADD KEY `fk_services_heyat1_idx` (`heyat_id`);

--
-- Indexes for table `share`
--
ALTER TABLE `share`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`id`);

-- Indexes for table `keyword`
--
ALTER TABLE `keyword`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `heyat`
--
ALTER TABLE `heyat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


--
-- Constraints for table `artist`
--
ALTER TABLE `artist`
ADD CONSTRAINT `fk_Artist_heyat1` FOREIGN KEY (`heyat_id`) REFERENCES `heyat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Artist_video1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `artist_has_artist`
--
ALTER TABLE `artist_has_artist`
ADD CONSTRAINT `fk_Artist_has_Artist_Artist1` FOREIGN KEY (`Artist_id_follower`) REFERENCES `artist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Artist_has_Artist_Artist2` FOREIGN KEY (`Artist_id_following`) REFERENCES `artist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fallow_artist`
--
ALTER TABLE `fallow_artist`
ADD CONSTRAINT `fk_fallow_artist_Artist1` FOREIGN KEY (`Artist_id`) REFERENCES `artist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_fallow_artist_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fallow_heyat`
--
ALTER TABLE `fallow_heyat`
ADD CONSTRAINT `fk_fallow_heyat_heyat1` FOREIGN KEY (`heyat_id`) REFERENCES `heyat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_fallow_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gallery_has_heyat`
--
ALTER TABLE `gallery_has_heyat`
ADD CONSTRAINT `fk_gallery_has_heyat_gallery1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_gallery_has_heyat_heyat1` FOREIGN KEY (`heyat_id`) REFERENCES `heyat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --------------------------------------------------------




--
-- Constraints for table `user_activationlink`
--
ALTER TABLE `user_activationlink`
  ADD CONSTRAINT `user_activationlink_ibfk_1` FOREIGN KEY (`fk_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`fk_role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`fk_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `url`
--
ALTER TABLE `url`
  ADD CONSTRAINT `url_ibfk_1` FOREIGN KEY (`fk_video_id`) REFERENCES `video` (`id`);

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`fk_heyat_id`) REFERENCES `heyat` (`id`),
  ADD CONSTRAINT `video_ibfk_2` FOREIGN KEY (`fk_uploader_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
