# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

from expert.handlers.expert_dashboard import *
# from expert.handlers.WebSocketHandler import *

url_patterns = [
    (r'/expert/dashboard/(\d+)', ExpertDashboardHandler, None, 'ExpertDashboardID'),
    # The urls for the current UI used
    # In another UI deleted or changed
    (r'/expert/frm1', ExpertDashboardfrm1Handler, None, 'ExpertDashboardFrm1'),
    (r'/expert/frm2', ExpertDashboardfrm2Handler, None, 'ExpertDashboardFrm2'),
]