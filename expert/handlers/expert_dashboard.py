# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler
from base.models.base_model import *
from pycket.session import SessionManager


class ExpertDashboardBaseHandler(WebBaseHandler):
    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super(ExpertDashboardBaseHandler, self).__init__(application, request, **kwargs)
        self.result = {
            'status': False,
            'message': '',
            'errors': [],
            'data': None
        }

    def on_finish(self):
        super(ExpertDashboardBaseHandler, self).on_finish()


class ExpertDashboardHandler(WebBaseHandler):
    def data_received(self, chunk):
        print("data_received")

    def get(self, *args, **kwargs):
        s = SessionManager(self)
        login_id = s.get('id')

        cat_id = args[0]
        if cat_id == '0':
            # LOADED VIDEOS LIST
            tf = 0
            cat_info = 0

        else:
            tf = 1
            cat_info = Video.select().where(Video.id == cat_id).get()
            print("done")

        # VIDEOS THAT THE USER HAS CHOSEN, NOT CONFIRMED!
        videos = []
        videos_table = Video.select().where(Video.fk_user_expert == login_id)
        for video in videos_table:
            if video.is_accepted == -1:
                videos.insert(len(videos), video)
        print(videos)

        self.render('export_dashboard.html', videos=videos, catInfo=cat_info, notification=0, tf=tf, select=-1)

    def post(self, *args, **kwargs):

        # s = SessionManager(self)
        # login_id = s.get('id')
        ################################login_id########
        # just for testtttttt
        expert_id = 10
        #########################################
        select = self.get_argument('select')
        videos = []
        videos_table = Video.select().where(Video.fk_user_expert == expert_id)
        for video in videos_table:
            if video.is_accepted == int(select):
                videos.insert(len(videos), video)
        print(videos)

        self.render('export_dashboard.html', videos=videos, catInfo=0, notification=0, tf=0, select=int(select))


class ExpertDashboardfrm1Handler(WebBaseHandler):
    """
    THE OBJECT OF THE HTML FILE AND RECORD DATABASE!
    """
    def post(self, *args, **kwargs):
        print("start post")
        video_id = int(self.get_argument('name_'))
        headline = self.get_argument('headline')
        headline_main = self.get_argument('headline_main')
        comment = self.get_argument('comment')
        expert_message = self.get_argument('expert_message')
        score = self.get_argument('score')
        accept = self.get_argument('accept')
        ###########################3#########
        # expert_id = EXPERT ID FROM SESSION
        expert_id = 10
        #####################################
        print("start update")
        vid = Video.select().where(Video.id == video_id).get()
        #
        # SEND NOTIFICATION TO USER
        #
        if accept == '0':
            if expert_message == "":
                Notification.create(video_name=vid.name, text="فیلم ارسالی شما توسط کارشناس رد شد!", is_read=0,
                                    fk_user=vid.fk_uploader)
                print("reject by system")
            else:
                Notification.create(video_name=vid.name, text=expert_message, is_read=0, fk_user=vid.fk_uploader)
                print("reject by expert")
            Video.update(headline=headline, headline_main=headline_main, comment=comment, score=score,
                         is_accepted=accept).where(Video.id == video_id).execute()
        elif accept == '1':
            if expert_message == "":
                Notification.create(video_name=vid.name, text="فیلم ارسالی شما توسط کارشناس تایید شد.", is_read=0,
                                    fk_user=vid.fk_uploader)
                print("accept by system")
            else:
                Notification.create(video_name=vid.name, text=expert_message, is_read=0, fk_user=vid.fk_uploader)
                print("accept by expert")
            Video.update(headline=headline, headline_main=headline_main, comment=comment, score=score, is_accepted=accept
                         ).where(Video.id == video_id).execute()
        else:
            Video.update(headline=headline, headline_main=headline_main, comment=comment, score=score).where(
                Video.id == video_id).execute()
            Notification.create(video_name=vid.name, text="فیلم ارسالی شما مورد باز بینی کارشناس قرار گرفت.", is_read=0, fk_user=vid.fk_uploader)
            print("edit by expert")

        print("post done!")
        self.redirect("/expert/dashboard/0")


class ExpertDashboardfrm2Handler(WebBaseHandler):
    """
    VIEW FILLED OF THAT LIST BY A CERTIFIED OR NOT!!!?
    ***********************************
    * THE EXPERT SYSTEM DOES NOT WORK *
    ***********************************
    """
    def data_received(self, chunk):
        print("data_received")

    def post(self, *args, **kwargs):
        select = self.get_argument('select')
        print(select)
        print("start done!")
        if select == '0':
            videos = Video.select().where(Video.is_accepted == 0)
            print("if")
        else:
            videos = Video.select().where(Video.is_accepted == 1)
            print("else")
        print("find video")
        self.render('export_dashboard.html', videos=videos, catInfo=0, notification=0, tf=0, select=select)
        # self.redirect("/export/dashboard/0", select=select)