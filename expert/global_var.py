# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

from config import Config
import os


class GlobalVar:
    def __init__(self):
        s = Config()
        self.server_path = os.path.join(s.applications_root, 'expert/')

        self.mysql_admin_db_name = 'db_htv'
        self.mysql_admin_host = '127.0.0.1'
        self.mysql_admin_user = 'root'
        self.mysql_admin_pass = ''
