# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web



from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class HeyatRequestHandler(WebBaseHandler):
    @authentication(32)
    def get(self, *args, **kwargs):
            page = int(self.get_argument('page',1))
            if page < 1:
                page = 1
            user = get_curent_info(self,"داشبورد" ,"مدیریت هیئت ها" ,"درخواست عضویت در هیئت")
            user_heyats = Heyat_has_user().select(Heyat_has_user.fk_heyat).where(Heyat_has_user.fk_user == user['login_user'] , ((Heyat_has_user.is_accept == 0)|(Heyat_has_user.is_accept==1)))
            heyats = Heyat.select().where(Heyat.is_active ==1, ~(Heyat.id << user_heyats)).paginate(page, 20)
            count = Heyat.select().where(Heyat.is_active ==1, ~(Heyat.id << user_heyats)).count()
            self.render('heyat/member_request.html', user=user, heyats=heyats, count=count, page=page)
    @authentication(32)
    def post(self, *args, **kwargs):
        print('post')
        heyat_id= self.get_argument('id')
        heyat = Heyat.select().where(Heyat.id == heyat_id).get()
        session =SessionManager(self)
        user_id = session.get('id')
        user = User.select().where(User.id == user_id).get()
        try:
            Heyat_has_user.select().where(Heyat_has_user.fk_user==user,Heyat_has_user.fk_heyat==heyat).get()
            self.redirect(self.reverse_url('member_request_heyat'))
        except:

            try:
                Heyat_has_user.create(fk_user = user , fk_heyat = heyat)
                text = "درخواست عضویت از طرف" +" "+ user.fore_name + " "  +user.sur_name+ " " + "دریافت شد"
                send_notification(self,user,heyat.fk_admin,text)
                self.redirect(self.reverse_url('request_heyat_search'))
            except:
                self.write("one error occur!")


class HeyatSearchRequestHandler(WebBaseHandler):
    @authentication(32)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","مدیریت هیئت ها","نتیجه درخواست")
        self.render('heyat/heyat_request_resualt.html' ,user=user)
        pass

    @authentication(32)
    def post(self, *args, **kwargs):
        print('post')
        session=SessionManager(self)
        user_id = session.get('id')
        user = User.select().where(User.id == user_id).get()

        ostan = self.get_argument('ostan')
        city= self.get_argument('city')
        addresses = Address().select().where(Address.province**("%"+ostan+"%"), Address.city**("%"+city+"%"))
        user_heyats = Heyat_has_user().select(Heyat_has_user.fk_heyat).where(Heyat_has_user.fk_user == user, ((Heyat_has_user.is_accept == 0)|(Heyat_has_user.is_accept==1)))
        heyats = Heyat().select().where(Heyat.fk_address << addresses ,~(Heyat.id << user_heyats))
        resualt = {}
        dict__ = {}
        dict__['count'] = heyats.count()
        resualt[0] = dict__
        i =1
        for item in heyats:
            dict__ ={}
            dict__['id'] = item.id
            dict__['name'] = item.name
            dict__['admin'] = item.fk_admin.fore_name + " " + item.fk_admin.sur_name+" "
            address = item.fk_address.province +" "+ item.fk_address.city +" "
            if item.fk_address.village!=None:
                address += item.fk_address.village + " "
            address += item.fk_address.address
            dict__['address'] = address
            resualt[i] = dict__
            i= i+1



        self.write(resualt)


class HeyatNewRequestHandler(WebBaseHandler):
    @authentication(30)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد" ,"مدیریت هیئت ها" ,"درخواست های جدید")
        heyat = Heyat.select().where(Heyat.fk_admin == user['login_user'])
        new_request = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat , Heyat_has_user.is_accept ==0)
        self.render('heyat/member_new_request.html', user=user, requests=new_request)
    @authentication(30)
    def post(self, *args, **kwargs):
        _id = self.get_argument('id')
        try:
            Heyat_has_user.update(is_accept =1).where(Heyat_has_user.id == _id).execute()
            heyat_request = Heyat_has_user().select().where(Heyat_has_user.id == _id).get()
            text = "درخواست عضویت شما در هیئت" + " " +heyat_request.fk_heyat.name+ " " + "با موفقیت تایید شد"
            send_notification(self,heyat_request.fk_heyat.fk_admin,heyat_request.fk_user,text)
            self.redirect(self.reverse_url('my_heyat_request'))
        except:
            self.write('one error occure!')

class HeyatNewRequestRejectHandler(WebBaseHandler):
    @authentication(30)
    def get(self, *args, **kwargs):
        pass


    @authentication(30)
    def post(self, *args, **kwargs):
        _id =self.get_argument('id')
        comment = self.get_argument('msg')
        user = User.select().where(User.id == _id).get()
        session = SessionManager(self)
        heyat= Heyat.select().where(Heyat.id == session.get('heyat')).get()
        print(heyat.id , user.id)
        # try:
        heyat_request = Heyat_has_user.select().where(Heyat_has_user.fk_user == user,Heyat_has_user.fk_heyat==heyat).get()
        Heyat_has_user.delete().where(Heyat_has_user.fk_user == user,Heyat_has_user.fk_heyat==heyat).execute()
        text = "درخواست عضویت شما در هیئت" + " " +heyat_request.fk_heyat.name+ " " + "رد شد"
        if comment!= None:
            text+="، علت رد درخواست:"+ " " + comment
        send_notification(self,heyat.fk_admin,heyat_request.fk_user,text)
        self.redirect(self.reverse_url('my_heyat_request'))
        # except:
        #     self.write('one error ocoure!')







