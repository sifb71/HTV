# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class Accept_CommentHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):
        try:
            _id = self.get_argument('id')
            Comment.update(status=1).where(Comment.id == _id).execute()
            self.write('ok')

        except:
            self.write('one error occur!')


class Delete_CommentHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):
        print('ppppd')
        # try:
        _id = self.get_argument('id')
        Comment.delete().where(Comment.id == _id).execute()
        self.write('ok')

        # except:
        #     self.write('one error occur!')
