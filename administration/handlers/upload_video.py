# -*- coding: UTF-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh, f_shariatmadari
# PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import re
import os
import tornado.web
import tornado.httputil
from base.handlers.base import WebBaseHandler
from config import Config
import os.path

from administration.classes.Public_actions import *

try:
    from base.classes.queue_handler import QueueHandler
    queue_flag = True
except ImportError:
    queue_flag = False
size = 12

def set_status(video_id, status):
    Video().update(status=status).where(Video.id == video_id).execute()


def get_heyat_path(video_id):
    heyat_id = Video().select().where(Video.id == video_id).get().fk_heyat.id

    if not os.path.exists(os.path.join(Config().upload_video, str(heyat_id))):
        os.mkdir(os.path.join(Config().upload_video, str(heyat_id)))

    return os.path.join(Config().upload_video, str(heyat_id))


def type_of_file(name):  # get file type from file_name
    name = name.split(".")[-1]
    return name


def create_name(user_id):
    u = User.select().where(User.id == user_id).get()
    num = Video().select().where(Video.fk_uploader == u, Video.status != -1).count()
    name = u.fore_name
    name += "_" + u.sur_name + str(num + 1)
    return name


class BaseHandler(WebBaseHandler):
    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)


class MediaBaseHandler(BaseHandler):
    def __init__(self, application, request, **kwargs):
        super(MediaBaseHandler, self).__init__(application, request, **kwargs)

    def on_finish(self, *args):
        pass


@tornado.web.stream_request_body
class StreamingHandler(MediaBaseHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.path = 'waiting_convert'
        self.user_id = self.get_current_user()
        self.bytes_read = 0

    def prepare(self):
        self.upload_directory = os.path.join(Config().applications_root, self.path)
        if not os.path.exists(self.upload_directory):
            os.mkdir(self.upload_directory)
        self.request.connection.set_max_body_size(99999999999)
        self.request.connection.set_body_timeout(10000)
        self.perifix = ['mp4', '3gp', 'flv', 'mpeg', 'webm', 'avi', 'mkv', 'ogv']
        self.flag = True
        self.sess = SessionManager(self)
    def data_received(self, chunk):
        try:
            chunk = re.sub(b'-----------------------------\d+\r\n', b'', chunk)
            # chunk = re.sub(b'------WebKitFormBoundary(.+)\n', b'', chunk , 1)
            temp = re.sub(b'------------------------------\d+--\r\n', b'', chunk, 1)

            if temp != chunk:  # find last chunk and remove this pattern from it
                chunk = re.sub(b'-----------------------------\d+--', b'', chunk, 1)

                chunk = chunk.strip()
                chunk = chunk + b"\n"

            all_match = re.findall(b'Content-Disposition:(.+)\n', chunk)

            if re.findall(b'Content-Type:(.+)\r', chunk):
                self.content_type = re.findall(b'Content-Type:(.+)\r', chunk)
                self.content_type = self.content_type[0]

            chunk = re.sub(b'Content-Disposition:(.+)\n', b'', chunk, 1)
            chunk = re.sub(b'Content-Type:(.+)\n', b'', chunk, 1)

            if all_match:  # find first chunk and remove this pattrrn from it and get name of file
                chunk = re.sub(b'-----------------------------\D+\n', b'', chunk, 1)
                chunk = chunk.strip()
                file_name = re.match(b'(.+)filename=(.+)', all_match[0])
                file_name = file_name.group(2)

                file_name = file_name.decode("utf-8")
                file_name = file_name.replace('\r', '')
                file_name = re.sub('"', '', file_name)
                self.type_file = type_of_file(file_name)
                if self.type_file.lower() not in self.perifix:
                    self.flag = False

                if self.flag != False:

                    user_id = self.sess.get('id')
                    video_id = self.sess.get('temp_id')

                    if video_id != None:
                        file_name = str(video_id) + '-' + create_name(user_id) + "." + self.type_file
                        self.file_name = file_name
                        file_address = os.path.normpath(os.path.join(self.upload_directory, file_name))
                        self.file_address = file_address

                    else:
                        self.flag = False

            if self.flag != False:
                if not os.path.exists(self.file_address):
                    self.file_ = open(self.file_address, 'wb').close()
                self.file_ = open(self.file_address , 'ab')
                self.file_.write(chunk)
                self.file_.close()
        except:
            pass

    @authentication(24)
    def get(self, *args, **kwargs):
        self.render("upload.html")
    @authentication(24)
    def post(self, *args, **kwargs):
        if self.flag == False:
            self.write('error')

        else:
            try:
                user_id = self.sess.get('id')
                name_video = create_name(user_id) + "." + self.type_file
                name_pic = self.sess.get('temp_pic_name')
                if name_pic != None:
                    self.sess.delete('temp_pic_name')
                Video().update(url=name_video, pic=name_pic, status=0).where(
                    Video.id == self.sess.get('temp_id')).execute()

                # when upload done successfully push uploaded video to "wait_for_convert" queue
                if queue_flag:
                    queue = QueueHandler('convert_to_webm', 'webm')
                    video_address = "{0},{1},{2}".format(Config().wait_for_convert,
                                                         self.file_name.split('.')[0], self.file_name.split('.')[1])
                    queue.push(video_address)

                self.sess.delete('temp_id')
                self.write('true')
            except:
                self.write('one error occur!')


class VideoUploadFinishHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","مدیریت ویدئوها","آپلود فیلم")
        self.render('finish_upload.html' ,user = user)


    def post(self, *args, **kwargs):
        global size
        size = int(self.get_argument('size'),0)
        if  size == 0:
            self.write('error')
        else:
            self.write('ok')
