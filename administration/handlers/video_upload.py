# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################


import tornado.web
from base.handlers.base import WebBaseHandler

from heyat.classes.video_keywords import *
from administration.classes.Public_actions import *


class VideoUploadHandler(WebBaseHandler):
    @authentication(24)
    def get(self, *args, **kwargs):

        user = get_curent_info(self, "داشبورد" ,  "مدیریت ویدئو ها","آپلود فیلم - گام اول")
        self.render('video_upload.html', user=user)
    @authentication(24)
    def post(self, *args, **kwargs):
        print('post')
        session = SessionManager(self)

        name = self.get_argument('name')
        headtxt = self.get_argument('headtxt')
        main_title = self.get_argument('maintxt')
        comment = self.get_argument('comment')
        type_ = self.get_argument('type')
        video_date = self.get_argument('date')
        keywords = self.get_argument('keywords')
        artists = self.get_argument('artists')



        province = self.get_argument('ostan').strip()
        city = self.get_argument('city').strip()
        village = self.get_argument('village').strip()
        address = self.get_argument('address').strip()

        if(province and city  and address):    #if enter a new address for video add it to address table
            fk_address = Address.create(province = province , city = city , village = village , address = address)

        else:
            fk_address = 8

        mem = User.select().where(User.id == session.get('id')).get()
        try:
            heyat = Heyat.select().where(Heyat.fk_admin == mem).get()
        except:
            try:
                temp = Heyat_has_user.select().where(Heyat_has_user.fk_user ==mem, Heyat_has_user.flag ==1).get()
                heyat = Heyat.select().where(Heyat.id == temp.fk_heyat).get()
                print(heyat.id)
            except:
                self.write('one error occure!')



        if name and main_title and  headtxt and type_ and video_date:
            id_ = Video.create(name=name,headline=headtxt, headline_main=main_title, comment=comment, type=type_,
                               fk_address=fk_address, fk_uploader=mem, fk_heyat=heyat, date=video_date ).get_id()

            session.set('temp_id', id_)
            if add_artist_to_video(artists, id_) and add_keyword_to_video(keywords, id_):

                self.redirect(self.reverse_url('upload_pic'))
            else:
                self.write('error')

        else:
            self.redirect(self.reverse_url('video_upload_1'))


class Artist_AutoLoadHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass;
    def post(self, *args, **kwargs):
        artist = self.get_argument('artist')
        role = Role().select().where((Role.id == 7) |(Role.id == 8) |(Role.id ==12) | (Role.id ==13))
        artists = User().select().where( User.fk_role << role , User.fore_name.concat(User.sur_name)**("%"+artist+"%"))\
            .limit(10)


        array = []
        for item in artists:
            array.append(item.fore_name + ' ' +item.sur_name)

        dict__ = {
            "key": array
        }

        self.write(dict__)

class Keyword_AoutoLoadHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass;
    def post(self, *args, **kwargs):
        keyword = self.get_argument('artist')
        keywords = Keyword().select().where(Keyword.name**("%"+keyword+"%"))\
            .order_by(Keyword.name)\
            .limit(10)
        array = []
        for item in keywords:
            array.append(item.name)

        dict__ = {
            "key": array
        }

        self.write(dict__)

class Heyat_AoutoLoadHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass;

    def post(self, *args, **kwargs):
        print('heyatloadding')
        heyat = self.get_argument('heyat')
        heyats = Heyat().select().where(Heyat.name**("%"+heyat+"%"))\
            .order_by(Heyat.name)\
            .limit(10)
        array = []
        for item in heyats:
            array.append(item.name)

        dict__ = {
            "key": array
        }

        self.write(dict__)
class User_AutoLoadHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass;

    def post(self, *args, **kwargs):
        print('usea loading')
        user = self.get_argument('user')
        role = Role().select().where((Role.id == 2) |(Role.id == 3) |(Role.id ==4))
        users = User().select().where(User.fk_role << role, User.fore_name.concat(User.sur_name)**("%"+user+"%"))\
            .limit(10)

        array = []
        for item in users:
            array.append(item.fore_name + ' ' +item.sur_name)

        dict__ = {
            "key": array
        }

        self.write(dict__)


