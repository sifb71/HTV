# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class AcceptHeyatHandler(WebBaseHandler):
    @authentication(30)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد" , "مدیریت هیئت ها" ,"لیست هیئت های منتظر تایید")
        heyats = Heyat.select().where(Heyat.is_active == 0)
        self.render('heyat/accept_heyat.html', user=user, heyats=heyats)
    @authentication(30)
    def post(self, *args, **kwargs):
        _id = self.get_argument('id')
        try:
            session = SessionManager(self)
            Heyat.update(is_active = 1).where(Heyat.id == _id).execute()
            heyat = Heyat.select().where(Heyat.id == _id).get()
            admin = User.select().where(User.id == session.get('id')).get()
            massage = "درخواست ثبت هیئت شما با موفقیت تایید شد. لطفا برای دسترسی داشتن به پنل مدیریتی کانال خود یک بار از حساب کاربری خود خارج و مجدداً وارد سایت شوید."
            send_notification(self,admin,heyat.fk_admin,massage)
            role = Role.select().where(Role.id == 2).get()
            User.update(fk_role = role).where(User.id == heyat.fk_admin).execute()
        except:
            self.write('one error Occur!')
        self.redirect(self.reverse_url("deactive_heyat_list"))


class DeleteHeyatHandler(WebBaseHandler):
    @authentication(30)
    def get(self, *args, **kwargs):
        pass
    @authentication(30)
    def post(self, *args, **kwargs):
        _id = self.get_argument('id')
        _text = self.get_argument('msg').strip()
        massege = "درخواست ثبت هیئت شما رد شد"
        if _text:
            massege += " "+"دلیل رد:" + " " +_text

        session = SessionManager(self)
        sender_id = session.get('id')
        heyat = Heyat.select().where(Heyat.id == _id).get()
        sender = User.select().where(User.id == sender_id).get()
        send_notification(self, sender,heyat.fk_admin,massege)
        Heyat.delete().where(Heyat.id == _id).execute()
        self.redirect(self.reverse_url("deactive_heyat_list"))

class Upload_PermissionHandler(WebBaseHandler):
    @authentication(33)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","مدیریت هیئت","دادن مجوز آپلود")
        heyat = Heyat.select().where(Heyat.fk_admin == user['login_user']).get()
        print(heyat.id)
        member = Heyat_has_user.select(Heyat_has_user.fk_user).where(Heyat_has_user.fk_heyat == heyat , Heyat_has_user.flag == 0)

        fname = self.get_argument('fname',"")
        lname = self.get_argument('lname',"")
        email = self.get_argument('email',"")
        phone = self.get_argument('phone',"")
        sort_by = self.get_argument('sort',"sur_name")
        sort_type = self.get_argument('type',"A")
        page = int(self.get_argument('page', 1))
        if page < 0:
            page = 1

        if sort_type == "A":
            users = User.select().where(User.id << member ,User.fore_name**("%"+fname+"%"),User.fk_role == 10,User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                        ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                        .order_by(SQL(sort_by).desc()).paginate(page, 20)
            sort_type = 'D'

        else:
            users = User.select().where(User.id << member ,User.fore_name**("%"+fname+"%"),User.fk_role == 10,User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                        ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                        .order_by(SQL(sort_by).asc()).paginate(page, 20)
            sort_type = 'A'


        count = User.select().where(User.id << member ,User.fk_role == 10 ,User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                        ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%")).count()

        filter=[fname , lname ,email,phone,sort_by,sort_type]
        self.render('users/upload_permossion.html',user=user, users=users,count=count,filter=filter,page=page)

class add_Upload_PermissionHandler(WebBaseHandler):
    @authentication(33)
    def get(self, *args, **kwargs):
        pass
    @authentication(33)
    def post(self, *args, **kwargs):
        user_id = self.get_argument('id')
        user = User.select().where(User.id == user_id).get()
        session = SessionManager(self)
        heyat = Heyat.select().where(Heyat.fk_admin == session.get('id')).get()
        role = Role.select().where(Role.id == 11).get()
        User.update(fk_role=role).where(User.id == user_id).execute()
        Heyat_has_user.update(flag = 1).where(Heyat_has_user.fk_heyat==heyat , Heyat_has_user.fk_user == user).execute()
        login_user = User.select().where(User.id == session.get('id')).get()
        text = "به شما اجازه آپلود فیلم در هیئت" +"  " +heyat.name+ "  "+"داده شد ، لطفا برای اعمال تغییرات یک بار از حساب کاربری خود خارج و مجددا وارد شوید. با تشکر"
        send_notification(self,login_user,user,text)
        self.redirect(self.reverse_url('user_upload_permision'))

class Uploaded_Permosin_users(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","مدیریت هیئت","دادن مجوز آپلود")
        heyat = Heyat.select().where(Heyat.fk_admin == user['login_user']).get()
        member = Heyat_has_user.select(Heyat_has_user.fk_user).where(Heyat_has_user.fk_heyat == heyat,Heyat_has_user.flag==1)

        fname = self.get_argument('fname',"")
        lname = self.get_argument('lname',"")
        email = self.get_argument('email',"")
        phone = self.get_argument('phone',"")
        sort_by = self.get_argument('sort',"sur_name")
        sort_type = self.get_argument('type',"A")
        page = int(self.get_argument('page', 1))
        if page < 0:
                page = 1

        if sort_type == "A":
                users = User.select().where(User.id << member ,User.fk_role == 11,User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                            ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                            .order_by(SQL(sort_by).desc()).paginate(page, 20)
                sort_type = 'D'

        else:
                users = User.select().where(User.id << member ,User.fk_role == 11,User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                            ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                            .order_by(SQL(sort_by).asc()).paginate(page, 20)
                sort_type = 'A'


        count = User.select().where(User.id << member ,(User.fk_role == 10) ,User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user['login_user'].id, User.is_deleted != 1\
                        ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%")).count()

        filter=[fname , lname ,email,phone,sort_by,sort_type]
        self.render('users/remove_upload_permossion.html',user=user, users=users,count=count,filter=filter,page=page)

    def post(self, *args, **kwargs):
        user_id = self.get_argument('id')
        session = SessionManager(self)
        user = User.select().where(User.id == user_id).get()
        heyat = Heyat.select().where(Heyat.fk_admin == session.get('id')).get()
        role = Role.select().where(Role.id == 10).get()
        User.update(fk_role=role).where(User.id == user_id).execute()
        Heyat_has_user.update(flag = 0).where(Heyat_has_user.fk_user==user , Heyat_has_user.fk_heyat==heyat).execute()
        login_user = User.select().where(User.id == session.get('id')).get()
        text = "اجازه آپلود فیلم در هیئت"+"  " +heyat.name+ "  "+"از شما گرفته شد ، برای اعمال تغییرات یک بار از حساب کاربری خود خارج و مجددا وارد شوید. با تشکر"
        send_notification(self,login_user,user,text)
        self.redirect(self.reverse_url('list_user_upload_permision'))


class Upload_Heyat_info(WebBaseHandler):
    @authentication(33)
    def get(self, *args, **kwargs):
        user = get_curent_info(self ,"داشبورد" ,  "مدیریت هیئت ",  "تکمیل اطلاعات")
        heyat = Heyat.select().where(Heyat.fk_admin == user['login_user']).get()
        self.render('heyat/update_heyat.html',user=user, heyat=heyat)

    @authentication(33)
    def post(self, *args, **kwargs):
        id = self.get_argument('id').strip()
        heyat = Heyat.select().where(Heyat.id == id).get()
        name = self.get_argument('name').strip()
        score = 0
        if name == "":
            name = heyat.name
        tablighat_date = self.get_argument('tablighatdate').strip()
        if tablighat_date == "":
            tablighat_date = heyat.org_register_date

        elif heyat.org_register_date == None:
            score = score +1

        start_date = self.get_argument('startdate').strip()
        if start_date == "":
            start_date= heyat.register_date
        mashar_date = self.get_argument('mashardate').strip()
        if mashar_date == "":
            mashar_date = heyat.mashaar_register_date
        elif heyat.mashaar_register_date == None:
            score = score +1

        province = self.get_argument('ostan',"").strip()
        city = self.get_argument('city',"").strip()
        village = self.get_argument('village',"").strip()
        if village == "":
            village = None
        address = self.get_argument('address',"").strip()

        if province and city and address:
            Address.update(province=province,city=city,village=village,address=address).where(Address.id == heyat.fk_address).execute()
        score = heyat.score + score
        Heyat.update(name=name,org_register_date=tablighat_date,register_date=start_date,mashaar_register_date=mashar_date,score=score).where(Heyat.id == id).execute()
        self.redirect(self.reverse_url('update_heyat'))




