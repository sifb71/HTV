#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class DomainListHandler(WebBaseHandler):
    @authentication(4)
    def get(self, *args, **kwargs):

        user = get_curent_info(self ,  "داشبورد" ,"حوزه های موضوعی" ,"کارشناس موضوعی")
        domain = Subject_Domain().select()
        self.render('expert/show_domains.html', user=user, domain=domain)
    @authentication(4)
    def post(self, *args, **kwargs):
        user = get_curent_info(self ,  "داشبورد" ,"حوزه های موضوعی" ,"کارشناس موضوعی")
        domain_id = self.get_argument('id')
        domain = Subject_Domain().select().where(Subject_Domain.id == domain_id).get()
        expert= Expert_has_Domain().select().where(Expert_has_Domain.fk_domain == domain)
        self.render('expert/expert_domain.html', user=user, expert=expert, domain=domain)


class Add_New_Expert(WebBaseHandler):
    @authentication(1)
    def get(self, *args, **kwargs):
        print('get')
        domain_id = self.get_argument('id')
        domain = Subject_Domain().select().where(Subject_Domain.id == domain_id).get()

        user = get_curent_info(self, "داشبورد" ,  "مدیریت نقش های سیستم", "افزودن کارشناس به حوزه" )


        expert_domain = Expert_has_Domain().select().where(Expert_has_Domain.fk_domain == domain)

        temp_array = []
        for item in expert_domain:
            temp_array.append(item.fk_user_expert.id)


        if (temp_array):
            expert = User().select().where(  ~(User.id  << temp_array) , ( User.fk_role == 6) | (User.fk_role == 1)  )#, User.fk_role == expert_role )
        else:
            expert = User().select().where((User.fk_role == 6) | (User.fk_role == 1))
        self.render('expert/add_expert_domain.html',user=user, domain=domain , expert=expert)

    @authentication(1)
    def post(self, *args, **kwargs):
       print('post')
       domain_id = self.get_argument('domain_id')
       expert_id = self.get_argument('expert_id')
       print(domain_id)

       domain = Subject_Domain().select().where(Subject_Domain.id == domain_id).get()
       expert = User().select().where(User.id == expert_id).get()
       try:
           Expert_has_Domain.get(fk_domain = domain , fk_user_expert = expert).get()
           self.redirect('/administration/add_expert?id=1')
       except:
           session = SessionManager(self)
           login_user = User.select().where(User.id == session.get('id')).get()
           text = "شما به کارشناسان حوزه "+" "+domain.name+" "+"اضافه شدید"
           send_notification(self,login_user,expert,text)
           Expert_has_Domain().create(fk_domain = domain , fk_user_expert = expert)
           self.redirect('/administration/add_expert?id=1')


class Delete_ExpertHandler(WebBaseHandler):
    @authentication(1)
    def get(self, *args, **kwargs):
        pass
    @authentication(1)
    def post(self, *args, **kwargs):
        expert_id = self.get_argument('id')
        domain_id = self.get_argument('domainid')
        domain = Subject_Domain.select().where(Subject_Domain.id == domain_id).get()
        expert = User().select().where(User.id == expert_id)
        Expert_has_Domain().delete().where(Expert_has_Domain.fk_user_expert == expert,Expert_has_Domain.fk_domain == domain).execute()
        session = SessionManager(self)
        login_user = User.select().where(User.id == session.get('id')).get()
        text = "شما از لیست کارشناسان حوزه "+" "+ domain.name+" "+".حذف شدید"
        send_notification(self,login_user,expert,text)

        self.redirect(self.reverse_url('domain_list'))

