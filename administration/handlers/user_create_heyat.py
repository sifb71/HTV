# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import mandrill
from base.handlers.base import WebBaseHandler

mandrill_client = mandrill.Mandrill('PcDu7RbpROprCYxpExCw4g')



from base.models.methods import *
from administration.classes.Public_actions import *
class CreateHeyatHandler(WebBaseHandler):
    @authentication(31)
    def get(self, *args, **kwargs):
            user = get_curent_info(self,"داشبورد", "مدیریت هیئت ها", "ساخت هیئت جدید")
            session = SessionManager(self)
            user_id =session.get('id')
            try:
                heyat = Heyat.select().where(Heyat.fk_admin == user['login_user']).get()
                self.redirect(self.reverse_url('error_create_heyat'))

            except:
                self.render('heyat/create_heyat.html',user=user)
    @authentication(31)
    def post(self, *args, **kwargs):
        s = SessionManager(self)
        member = User().select().where(User.id == s.get('id')).get()
        try:
            print('try')
            heyat = Heyat.select().where(Heyat.fk_admin == member).get()
            print('ok')
            self.redirect(self.reverse_url('error_create_heyat'))
        except:
            name = self.get_argument('name').strip()
            tablighat_date = self.get_argument('tablighatdate').strip()
            start_date = self.get_argument('startdate').strip()
            mashar_date = self.get_argument('mashardate').strip()
            phone = self.get_argument('phone').strip()
            print(start_date)

            province = self.get_argument('ostan').strip()
            city = self.get_argument('city').strip()
            village = self.get_argument('village').strip()
            address = self.get_argument('address').strip()
            score = 2

            if name and province and city and address and phone and start_date:
                if tablighat_date is "":
                    tablighat_date = None
                    score = score- 1
                if mashar_date == "":
                    score = score - 1
                    mashar_date = None

                if village == "":
                    village = None

                address = CreateAddress(province, city, village, address)
                if address != -1:
                    Heyat.create(name=name, org_register_date=tablighat_date, register_date=start_date, mashaar_register_date=mashar_date,
                                 statute="", fk_address=address, fk_admin=member,score=score)
                    User.update(phone=phone).where(User.id==member).execute()
                    massage = "درخواست ساخت هیئت جدید از" +" "+ member.fore_name + " " + member.sur_name +" "+ "دریافت شد."
                    send_Nofification_to_super_admin(member, massage)
                    self.redirect(self.reverse_url('finish_create_heyat'))

                else:
                    self.write('one error occur')

            else:
                self.write('Enter All Arguments')

class FinishCreateHeyatHandler(WebBaseHandler):
    @authentication(31)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","مدیریت هیئت ","ساخت هیئت جدید")
        self.render('heyat/finish_create_heyat.html', user=user)
        pass
    @authentication(31)
    def post(self, *args, **kwargs):
        pass
class CreateHeyat_Error_Handler(WebBaseHandler):
    @authentication(31)
    def get(self, *args, **kwargs):

        user = get_curent_info(self,"داشبورد","مدیریت هیئت ","ساخت هیئت جدید")
        self.render('heyat/create_heyat_errorpage.html', user=user)
    @authentication(31)
    def post(self, *args, **kwargs):
        pass

class UpdateHeyatHandler(WebBaseHandler):
    @authentication(31)
    def get(self, *args, **kwargs):
            user = get_curent_info(self,"داشبورد", "مدیریت هیئت ها", "ساخت هیئت جدید")
            session = SessionManager(self)
            user_id =session.get('id')
            try:
                heyat = Heyat.select().where(Heyat.fk_admin == user['login_user']).get()
                self.redirect(self.reverse_url('error_create_heyat'))

            except:
                self.render('heyat/create_heyat.html',user=user)
    @authentication(31)
    def post(self, *args, **kwargs):
        s = SessionManager(self)
        member = User().select().where(User.id == s.get('id')).get()
        try:
            print('try')
            heyat = Heyat.select().where(Heyat.fk_admin == member).get()
            print('ok')
            self.redirect(self.reverse_url('error_create_heyat'))
        except:
            name = self.get_argument('name').strip()
            tablighat_date = self.get_argument('tablighatdate').strip()
            start_date = self.get_argument('startdate').strip()
            mashar_date = self.get_argument('mashardate').strip()
            phone = self.get_argument('phone').strip()
            print(start_date)

            province = self.get_argument('ostan').strip()
            city = self.get_argument('city').strip()
            village = self.get_argument('village').strip()
            address = self.get_argument('address').strip()
            score = 2

            if name and province and city and address and phone and start_date:
                if tablighat_date is "":
                    tablighat_date = None
                    score = score- 1
                if mashar_date == "":
                    score = score - 1
                    mashar_date = None

                if village == "":
                    village = None

                address = CreateAddress(province, city, village, address)
                if address != -1:
                    Heyat.create(name=name, org_register_date=tablighat_date, register_date=start_date, mashaar_register_date=mashar_date,
                                 statute="", fk_address=address, fk_admin=member,score=score)
                    User.update(phone=phone).where(User.id==member).execute()
                    massage = "درخواست ساخت هیئت جدید از" +" "+ member.fore_name + " " + member.sur_name +" "+ "دریافت شد."
                    send_Nofification_to_super_admin(member, massage)
                    self.redirect(self.reverse_url('finish_create_heyat'))

                else:
                    self.write('one error occur')

            else:
                self.write('Enter All Arguments')

