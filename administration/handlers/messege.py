# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class Inbox_MessegeHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        page = int(self.get_argument('page',1))
        if page <0:
            page = 1

        user = get_curent_info(self,"داشبورد","پیام ها","صندوق ورودی")
        message = Notification.select().where(Notification.fk_receiver == user['login_user']) .order_by(Notification.inserted_date.desc())\
        .paginate(page,10)
        count = Notification.select().where(Notification.fk_receiver == user['login_user']).count()
        print(count)

        for item in message:
            Notification.update(is_read =1).where(Notification.id == item.id).execute()

        self.render('inbox.html',user=user, message=message, count=count, page=page)

class Delete_MessegeHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):
        session = SessionManager(self)
        user_id = session.get('id')
        user = User.select().where(User.id == user_id).get()
        message = Notification.select().where(Notification.fk_receiver == user)

        for item in message:
            msg = self.get_argument(str(item.id) , None)
            print(msg , item.id)
            if msg != None:
                Notification.delete().where(Notification.id == item.id).execute()

        self.redirect(self.reverse_url('message_inbox'))
