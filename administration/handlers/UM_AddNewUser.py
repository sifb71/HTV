###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import hashlib
import string
import random

from base.classes import MailClass
from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class Add_UserHandler(WebBaseHandler):
    @authentication(12)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,u"داشبورد" , "داشبورد"  , "افزودن کاربر")
        roles = Role.select()

        self.render('users/create_user.html', user=user, roles=roles)
    @authentication(12)
    def post(self, *args, **kwargs):
        print("post")
        f_name = self.get_argument('fname')
        l_name = self.get_argument('lname')
        phone = self.get_argument('phone')
        email = self.get_argument('email')
        role = self.get_argument('action')
        resualt ={}
        flag = True

        if f_name and l_name and email and phone:
            try:
                mem = User.select().where(User.email == email).get()
                if mem:
                    resualt['resualt'] = "error"
                    resualt['txt'] = "ایمیل وارد شده قبلا در سیستم ثبت شده است"
                    flag = False

            except:
                flag = True

            if flag:
                pass_w =''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(5))
                password = pass_w
                m = hashlib.md5()
                m.update(pass_w.encode('utf-8'))
                pass_w = m.hexdigest()

                role_ = Role().select().where(Role.id == role).get()
                User().create(fore_name=f_name, sur_name=l_name, password=pass_w, email=email, is_active=1,phone=phone,
                              fk_role=role_, pic='user.png')

                to = [{'email': email, 'name': f_name + " " + l_name, 'type': 'to'}]
                txt = "به جمع کاربران هیئت تی وی خوش مدید . نام کاربری شما:" + email + "****" + "پسورد" + password

                MailClass.HeyatTVMail('admin@HeayTv.ir', 'هیئت تی وی', 'خوش آمدید', '', to).send(txt)
                resualt['resualt'] = "ok"
                resualt['txt'] = "کاربر جدید با موفقیت ساخته شد"


            else:
                resualt['resualt'] = "error"
                resualt['txt'] = "ایمیل وارد شده قبلا در سیستم ثبت شده است"

        else:
            resualt['resualt'] = "error"
            resualt['txt'] = "لطفا تمامی ورودی ها را وارد کنید"
        self.write(resualt)
