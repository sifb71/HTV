# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class AddUserRoleHandler(WebBaseHandler):
    @authentication(11)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد" ,"مدیریت کاربران  " , "لیست کاربران" )
        session = SessionManager(self)
        user_id = session.get('id')
        user_role_id = session.get('role')
        heyat_id = session.get('heyat')
        user_role = Role().select().where(Role.id == user_role_id).get()
        role_level = user_role.level
        users = None
        fname = self.get_argument('fname',"")
        lname = self.get_argument('lname',"")
        email = self.get_argument('email',"")
        phone = self.get_argument('phone',"")
        sort_by = self.get_argument('sort',"sur_name")
        sort_type = self.get_argument('type',"A")
        page = int(self.get_argument('page', 1))
        print(sort_type)

        if page < 0:
            page = 1



        if role_level == 1:  # if curent user's level is Kol
            print('l1')
            # select all other user
            if sort_type == "A":
                temp_role = Role.select().where(Role.id == 13).get()
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                    ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                    .order_by(SQL(sort_by).desc()).paginate(page, 20)

                sort_type = 'D'

            else:
                temp_role = Role.select().where(Role.id == 13).get()
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                .order_by(SQL(sort_by).asc()).paginate(page, 20)
                sort_type = 'A'

        elif role_level == 2:  # if curent user's level is Province


            if user['login_user'].fk_address is not None:
                # select all user in my province
                address = Address().select().where(Address.province == user['login_user'].fk_address.province)
                temp_role = Role.select().where(Role.id == 13).get()

                if sort_type == "A":

                    users = User().select() \
                      .where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.fk_address << address, User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                      ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                      .order_by(SQL(sort_by).desc()).paginate(page, 20)


                    sort_type = "D"

                else:

                    users = User().select() \
                    .where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.fk_address << address, User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                    ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                    .order_by(SQL(sort_by).asc()).paginate(page, 20)
                    sort_type = "A"


            else:
                print('l2 else')
                print('user do not have address')
                self.write('user do not have address')

        else:  # if curent user's level is Heyat
            print('l3')
            heyat = Heyat().select().where(Heyat.id == heyat_id)
            heyat_member = Heyat_has_user().select(Heyat_has_user.fk_user) \
                .where(Heyat_has_user.fk_heyat == heyat)
            # select all user who are member in my Heyat
            temp_role = Role.select().where(Role.id == 13).get()
            if sort_type == "A":
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.is_deleted != 1 , User.fk_role != temp_role \
                ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                .order_by(SQL(sort_by).desc()).paginate(page, 20)
                sort_type = "D"
            else:

                 users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.is_deleted != 1 , User.fk_role != temp_role \
                  ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                  .order_by(SQL(sort_by).asc()).paginate(page, 20)
                 sort_type = "A"

        count = users.count()
        filter = [fname , lname ,email,phone,sort_by,sort_type]
        roles = Role.select().where(Role.id != 13)
        self.render('users/addrole.html', user=user, users=users , page=page , count=count, filter=filter,roles = roles)

    @authentication(11)
    def post(self, *args, **kwargs):

        flag = True
        newrole = self.get_argument('roles')
        role = Role.select().where(Role.id == int(newrole.split('_',)[1])).get()
        print(role.id)
        if role.id ==2 or role.id ==3 or role.id == 4:
            user = User.select().where(User.id == int(newrole.split('_')[0]) ).get()
            try:
                Heyat.select().where(Heyat.fk_admin == user).get()
            except:
                flag = False
        if flag == True:
            User.update(fk_role=role).where(User.id == int(newrole.split('_')[0])).execute()
            session = SessionManager(self)
            sender = User.select().where(User.id== session.get('id')).get()
            resiver =User.select().where(User.id == int(newrole.split('_')[0])).get()
            text="نقش شما به"+ " " +role.name +" " +"تغییر یافت، لطفا برای اعمال تغییرات یک بار از حساب کاربری خود خارج ومجددا وارد شوید.باتشکر";
            send_notification(self,sender,resiver,text)
            self.redirect(self.redirect('add_user_role'))
        else:
            login_user = get_curent_info(self, "داشبورد" ,"مدیریت کاربران  " , "لیست کاربران" )
            self.render('roles/role_errorpage.html',user=login_user, name = user.fore_name + " " +user.sur_name)

