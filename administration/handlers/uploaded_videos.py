# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from multiprocessing import Lock
from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class SubjectiveJudgementHandler(WebBaseHandler):
    @authentication(13)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد" ,  "کارشناسی", "انتخاب فیلم برای کارشناسی موضوعی" )
        session = SessionManager(self)
        user_role = session.get('role')

        if user_role == 1:
            print("admin")
            flag = 1
            sub = Subject_Domain().select().where(Subject_Domain.id !=9 ).get()
            video = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_subject_domain == sub ,SubjectDomain_has_Video.fk_expert== None)
            videos2 = Video.select().where(Video.id << video)
            subjects = Subject_Domain.select().where(Subject_Domain.id != 9)


        else:
            print("experts")
            flag = 0
            sub_domain = Expert_has_Domain.select(Expert_has_Domain.fk_domain).where(Expert_has_Domain.fk_user_expert == user['login_user'])
            try:
                sub = Sub_Domain.select().where(Sub_Domain.id<< sub_domain).get()
                seb_id = sub.id
                video = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_subject_domain == sub , SubjectDomain_has_Video.fk_expert == None)
                videos2 = Video.select().where(Video.id << video)
                subjects = Subject_Domain.select().where(Subject_Domain.id << sub_domain)
            except:
                sub = []
                seb_id = -1
                videos2 = []
                subjects=[]


        expert_domain = Expert_has_Domain.select(Expert_has_Domain.fk_user_expert).where(Expert_has_Domain.fk_domain == sub)
        experts = User.select().where(User.id << expert_domain)
        self.render('expert/subject_expert.html', user=user, videos=videos2, experts=experts, flag=flag, subjects=subjects, subject= seb_id)

    @authentication(13)
    def post(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد" ,  "کارشناسی", "انتخاب فیلم برای کارشناسی موضوعی" )
        select = self.get_argument('chose_subjects',-1)
        session = SessionManager(self)
        user_role = session.get('role')
        if user_role == 1:
            flag = 1
            try:
                sub = Subject_Domain.select().where(Subject_Domain.id == select).get()
                sub_id =-1
                videos = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_subject_domain == sub , SubjectDomain_has_Video.fk_expert == None)
                videos2 = Video.select().where(Video.id << videos)
                subjects = Subject_Domain.select().where(Subject_Domain.id != 9)
            except:
                sub = []
                seb_id = -1
                videos2 = []
                subjects=[]

        else:
            print("experts")
            flag = 0
            sub_domain = Expert_has_Domain.select(Expert_has_Domain.fk_domain).where(Expert_has_Domain.fk_user_expert == user['login_user'])
            select = self.get_argument('chose_subjects',-1)
            try:
                sub = Subject_Domain.select().where(Subject_Domain.id == select).get()
                videos = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_subject_domain == sub , SubjectDomain_has_Video.fk_expert==None)
                videos2 = Video.select().where(Video.id << videos)
                subjects = Subject_Domain.select().where(Subject_Domain.id << sub_domain)
            except:
                sub = []
                seb_id = -1
                videos2 = []
                subjects=[]


        expert_domain = Expert_has_Domain.select(Expert_has_Domain.fk_user_expert).where(Expert_has_Domain.fk_domain == sub)
        experts = User.select().where(User.id << expert_domain)
        self.render('expert/subject_expert.html', user=user, videos=videos2, experts=experts, flag=flag, subjects=subjects,
                    subject=seb_id)


class UploadedListHandler(WebBaseHandler):
    @authentication(8)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد" ,"کارشناسی" , "انتخاب فیلم برای کارشناسی اولیه" )
        session = SessionManager(self)
        user_role =session.get('role')
        user_id = session.get('id')
        _role = Role.select().where(Role.id == 5)
        experts = User.select().where(User.fk_role == _role)
        videos = Video.select().where(Video.fk_user_expert == None, Video.status == 1)
        if user_role == 1:
            flag = 1
        else:
            flag = 0
        self.render('expert/uploaded_list.html',user=user, videos=videos, experts=experts, flag=flag)

    @authentication(8)
    def post(self, *args, **kwargs):
        session = SessionManager(self)
        expert_id = session.get('id')
        role_ = session.get('role')
        if role_ == 1:
            expert_id = int(self.get_argument('chose_expert'))

        expert = User.select().where(User.id == expert_id).get()
        mutex = Lock()
        with mutex:
            for video in Video.select().where(Video.fk_user_expert == None, Video.status == 1):
                if self.get_argument(str(video.id), None):
                    Video.update(fk_user_expert=expert).where(Video.id == video.id).execute()

        self.redirect(self.reverse_url("uploaded_videos"))




class SubjectiveJudgementHandler_(WebBaseHandler):
    @authentication(13)
    def get(self, *args, **kwargs):
        pass
    @authentication(13)
    def post(self, *args, **kwargs):
        print("start post")
        session = SessionManager(self)
        expert_id = session.get('id')
        role_ = session.get('role')
        if role_ == 1:
            expert_id = int(self.get_argument('chose_expert'))
            expert = User.select().where(User.id == expert_id)
        else:
            print('expert')
            expert = User.select().where(User.id == expert_id).get()


        subject = self.get_argument('chose_subjects')
        try:
            subject_domain = Subject_Domain.select().where(Subject_Domain.id == subject).get()

            video = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_subject_domain == subject_domain)
            video2 = Video.select().where(Video.id << video)

            mutex = Lock()
            with mutex:
                for item in video2:
                    vid = self.get_argument(str(item.id), None)
                    if vid:
                        SubjectDomain_has_Video.update(fk_expert=expert).where(SubjectDomain_has_Video.fk_video == item.id ,SubjectDomain_has_Video.fk_subject_domain==subject_domain).execute()
            self.redirect(self.reverse_url("subject_videos"))
        except:
            self.redirect(self.reverse_url("subject_videos"))






class MyUploadedVideosHandler(WebBaseHandler):
    @authentication(17)
    def get(self, *args, **kwargs):
        try:
            page= int(self.get_argument('page',1))
            sort_by = self.get_argument('by' ,'name')
            sort_type = self.get_argument('type' ,'A')
            if page < 1:
                page = 1
            user = get_curent_info(self,"داشبورد","مدیریت ویدئوها","فیلم های من")
            heyat = Heyat.select().where(Heyat.fk_admin == user['login_user'])

            if sort_type == "A":
                videos = Video.select(Video,fn.Count(Video_Has_Comment.id).alias('video_comment')).where(Video.fk_heyat << heyat , Video.status!= -1 )\
                    .join(Video_Has_Comment, JOIN_LEFT_OUTER) \
                    .group_by(Video.id) \
                    .order_by(SQL(sort_by).desc()).paginate(page, 20)
                sort_type = "D"
            else:
                videos = Video.select(Video,fn.Count(Video_Has_Comment.id).alias('comment')).where(Video.fk_heyat << heyat , Video.status!= -1,Video_Has_Comment.status==0)\
                    .join(Video_Has_Comment, JOIN_LEFT_OUTER) \
                    .group_by(Video.id) \
                    .order_by(SQL(sort_by).asc()).paginate(page,20)
                sort_type = "A"
            filter=[sort_by,sort_type]
            count = Video.select().where(Video.fk_heyat << heyat , Video.status!= -1).count()
            self.render('heyat/myuploaded.html', user=user, videos=videos, count=count,page=page,filter=filter)
        except:
            self.write('one error Occure')
    @authentication(17)
    def post(self, *args, **kwargs):
        pass
