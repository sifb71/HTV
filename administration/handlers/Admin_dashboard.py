# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: mohammad hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
###############################################################################
import tornado.web
from administration.classes.Public_actions import *
import hashlib
import os
from base.handlers.base import WebBaseHandler
from config import Config
import random
# import Image
from PIL import Image
def create_name(user_id):
    u = User.select().where(User.id == user_id).get()
    name = u.sur_name + str(random.randint(100000,999999))
    return name

class DashboardHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        print('get')
        curent_user = get_curent_info(self, 'داشبورد', 'داشبورد', 'پروفایل ')
        info = {}
        info['flag'] = 0
        report_id = [1, 2, 3, 4, 14, 15]
        role = curent_user['login_user'].fk_role
        heyat = {}
        heyat_member = Heyat_has_user.select().where(Heyat_has_user.fk_user == curent_user['login_user'] , Heyat_has_user.is_accept == 1)
        if role.id in report_id:
            info['flag'] = 1
            today = datetime.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
            if role.id == 1 or role.id == 15:

                print('super admin')
                info['upload'] = Video.select().where(Video.inserted_date > today).count()
                info['download'] = 0
                info['member'] = User.select().where(User.inserted_date > today,User.fk_role!= 13).count()
                info['comment'] = Comment.select().where(Comment.inserted_date > today).count()

            elif role.id == 2 or role.id == 3 or role.id == 4:
                heyat['heyat'] = Heyat.select().where(Heyat.fk_admin == curent_user['login_user']).get()
                info['upload'] = Video.select().where(Video.fk_heyat == heyat['heyat'],
                                                      Video.date  > today).count()
                info['download'] = 0
                info['member'] = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat['heyat'],
                                                               Heyat_has_user.inserted_date  > today).count()
                info['comment'] = 0
                heyat['member'] = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat['heyat'],Heyat_has_user.is_accept == 1).count()
                heyat['video'] = Video.select().where(Video.fk_heyat == heyat['heyat']).count()
                heyat['new_member'] = Heyat_has_user.select().where(Heyat_has_user.is_accept == 0,Heyat_has_user.fk_heyat==heyat['heyat']).count()


            elif role.id == 14:
                user_province = curent_user['login_user'].fk_address.province
                addresses = Address.select().where(Address.province == user_province)
                member = User.select().where(User.fk_address << addresses)
                info['member'] = member.count()
                info['download'] = 0
                info['comment'] = 0
                heyats = Heyat.select().where(Heyat.fk_admin << member)
                info['upload'] = Video.select().where(Video.fk_heyat << heyats,
                                                      Video.date  > today).count()
        for i in curent_user['action']:
            print(i.fk_action);
        self.render('profile/profile.html', user=curent_user, info=info,heyat=heyat, member=heyat_member)


class HeyatModifyHandler(WebBaseHandler):
    @authentication(5)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد", "مدیریت هیئت ها", "لیست هیئت ها")
        page = int(self.get_argument('page', 1))
        user_name = self.get_argument('user', "")
        heyat = self.get_argument('heyat', "")
        ostan = self.get_argument('ostan', "")
        city = self.get_argument('city', "")
        sort_by = self.get_argument('sort', "name")
        sort_type = self.get_argument('type', "A")

        if page < 0:
            page = 1

        adresses = Address.select().where(Address.province ** ("%" + ostan + "%"), Address.city ** ("%" + city + "%"))

        if user_name != "":
            users = User().select().where((SQL('CONCAT (fore_name," ",sur_name) = %s', user_name)))
        else:
            users = User().select()
        if sort_type == 'A':
            heyats = Heyat.select(Heyat, fn.Count(Heyat_has_user.id).alias('member') ).where(
                Heyat.is_deleted == 0, Heyat.name ** ("%" + heyat + "%"),
                Heyat.fk_address << adresses, Heyat.fk_admin << users) \
                .join(Heyat_has_user, JOIN_LEFT_OUTER) \
                .group_by(Heyat) \
                .order_by(SQL(sort_by).desc()).paginate(page, 20)
            sort_type = 'D'
        else:
            heyats = Heyat.select(Heyat, fn.Count(Heyat_has_user.fk_user).alias('member') ).where(
                Heyat.is_deleted == 0, Heyat.name ** ("%" + heyat + "%"),
                Heyat.fk_address << adresses, Heyat.fk_admin << users) \
                .join(Heyat_has_user, JOIN_LEFT_OUTER) \
                .group_by(Heyat.id) \
                .order_by(SQL(sort_by).asc()).paginate(page, 20)
            sort_type = 'A'

        filter = [user_name, heyat, ostan, city, sort_by, sort_type]

        count = Heyat.select().where(Heyat.is_deleted == 0, Heyat.name ** ("%" + heyat + "%"),
                                     Heyat.fk_address << adresses, Heyat.fk_admin << users).count()
        self.render('heyat/modify.html', user=user, heyats=heyats, page=page, count=count, filter=filter)


class HM_ModifyHeyatLevelHandler(WebBaseHandler):
    @authentication(5)
    def get(self, *args, **kwargs):
        self.redirect(self.reverse_url('heyat_modify'))

    @authentication(5)
    def post(self, *args, **kwargs):
        value = self.get_argument('level')
        id_ = self.get_body_argument('id')
        print(value)
        print(id_)
        if value == '-1':
            Heyat.update(is_deleted=1).where(Heyat.id == id_).execute()
        else:
            Heyat.update(level=value).where(Heyat.id == id_).execute()
        self.redirect(self.reverse_url('heyat_modify'))

class Edit_infoHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):
        print('post')
        fname = self.get_argument('fname').strip()
        lname = self.get_argument('lname').strip()
        phone = self.get_argument('phone').strip()
        about = self.get_argument('about').strip()



        if fname and lname:
            session = SessionManager(self)
            if phone == "":
                user = User.select().where(User.id == session.get('id') ).get()
                if user.phone == None:
                    phone = " "
                else:
                    phone = user.phone
            User.update(fore_name=fname, sur_name=lname, phone=phone, about_me=about).where(User.id == session.get('id')).execute()
            session.set('fname', fname)
            session.set('lname', lname)

            if self.request.files != {}:
                pic = self.request.files['pic'][0]
                extension = os.path.splitext(pic['filename'])[1].lower()
                upload_folder = os.path.join(Config().applications_root, 'static', 'images', 'users')
                if not os.path.exists(upload_folder):
                    os.makedirs(upload_folder)
                if extension in ['.jpg', '.png','.bmp', '.jpeg']:
                    photo_name = create_name(session.get('id'))+ extension
                    full_name = os.path.join(upload_folder, photo_name)
                    output = open(full_name, 'wb')
                    output.write(pic['body'])
                    output.close()
                    User.update(pic=photo_name).where(User.id == session.get('id')).execute()
                    size= 128,128
                    im = Image.open(full_name)
                    im.thumbnail(size, Image.ANTIALIAS)
                    im.save(full_name, "JPEG")
                    print(full_name)





        self.redirect(self.reverse_url('dashboard'))


class change_passHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):
        print('post')
        old_pass = self.get_argument('oldpass')
        new_pass = self.get_argument('newpass')

        session = SessionManager(self)
        user_id = session.get('id')
        try:
            user = User.select().where(User.id == user_id).get()
            m = hashlib.md5()
            m.update(old_pass.encode('utf-8'))
            old_pass = m.hexdigest()
            if old_pass != user.password:
                self.write('0')
            else:
                m = hashlib.md5()
                m.update(new_pass.encode('utf-8'))
                new_pass = m.hexdigest()

                User.update(password=new_pass).where(User.id == user_id).execute()
                self.write('1')
        except:
            self.write('-1')


class Edit_Heyat_logoHandler(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass

    @authentication(25)
    def post(self, *args, **kwargs):
            session = SessionManager(self)
            heyat_id = session.get('heyat')
            heyat = Heyat.select().where(Heyat.id == heyat_id).get()

            if self.request.files != {}:
                pic = self.request.files['logo'][0]
                extension = os.path.splitext(pic['filename'])[1].lower()
                upload_folder = os.path.join(Config().applications_root, 'static', 'images', 'heyats')
                if not os.path.exists(upload_folder):
                    os.makedirs(upload_folder)
                if extension in ['.jpg', '.png', '.gif', '.bmp', '.jpeg']:
                    photo_name = heyat.name + str(random.randint(100000,999999))
                    full_name = os.path.join(upload_folder, photo_name + extension)
                    output = open(full_name, 'wb')
                    output.write(pic['body'])
                    output.close()
                    user = User.select().where(User.id == session.get('id') ).get()
                    photo_name = photo_name + extension
                    Heyat.update(logo=photo_name).where(Heyat.fk_admin == user ).execute()
                    size= 128,128
                    im = Image.open(full_name)
                    im.thumbnail(size, Image.ANTIALIAS)
                    im.save(full_name, "JPEG")



            self.redirect(self.reverse_url('dashboard'))







