# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class TotalJudgementHandler(WebBaseHandler):
    @authentication(8)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد","کارشناسی اولیه" ,"انتخاب فیلم برای کارشناسی اولیه" )
        videos = Video.select().where(Video.fk_user_expert == user['login_user'] , Video.status==1)
        self.render('expert/expert_panels_general.html', user=user ,videos=videos )
    @authentication(8)
    def post(self, *args, **kwargs):
        pass


class SubjectJudgementHandler(WebBaseHandler):
    @authentication(13)
    def get(self, *args, **kwargs):
        user = get_curent_info(self ,"داشبورد" , "داشبورد" ,"انتخاب فیلم برای کارشناسی موضوعی")
        sub = Expert_has_Domain.select(Expert_has_Domain.fk_domain).where(Expert_has_Domain.fk_user_expert == user['login_user'])
        sub_domain = Subject_Domain.select().where(Subject_Domain.id << sub)
        try:
            sub = sub_domain.get()
            sub_id = sub.id
            videos = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video)\
                .where(SubjectDomain_has_Video.fk_expert == user['login_user'],SubjectDomain_has_Video.fk_subject_domain==sub, SubjectDomain_has_Video.status == 0)
            videos2 = Video.select().where(Video.id << videos )

        except:
            videos2 =[]
            sub_domain = []
            sub_id = -1

        self.render('expert/expert_panels_subject.html', user=user, videos=videos2, subjects=sub_domain , subject = sub_id)

    @authentication(13)
    def post(self, *args, **kwargs):
        user = get_curent_info(self ,"داشبورد" , "داشبورد" ,"انتخاب فیلم برای کارشناسی موضوعی")
        sub = Expert_has_Domain.select(Expert_has_Domain.fk_domain).where(Expert_has_Domain.fk_user_expert == user['login_user'])
        sub_domain = Subject_Domain.select().where(Subject_Domain.id << sub)
        try:
            sub = Subject_Domain.select().where(Subject_Domain.id == self.get_argument('chose_subjects' , None)).get()
            videos = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video)\
                .where(SubjectDomain_has_Video.fk_expert == user['login_user'],SubjectDomain_has_Video.fk_subject_domain==sub, SubjectDomain_has_Video.status == 0)
            videos2 = Video.select().where(Video.id << videos )
            sub_id=  sub.id
        except:
            sub=[]
            sub_id = -1
            videos2=[]
        self.render('expert/expert_panels_subject.html', user=user, videos=videos2, subjects=sub_domain , subject = sub_id)


class JudgedHandler(WebBaseHandler):
    @authentication(29)
    def get(self, *args, **kwargs):
        user = get_curent_info(self ,"داشبورد" ,  "کارشناسی ",  "فیلم های کارشناسی شده")
        videos = Video.select().where(Video.fk_user_expert == user['login_user'], ((Video.status == 2)|(Video.status ==3)))
        self.render('expert/judged.html', user=user, videos=videos, selected=2, expert =1)
    @authentication(29)
    def post(self, *args, **kwargs):
        user = get_curent_info(self ,"داشبورد" ,  "کارشناسی ",  "فیلم های کارشناسی شده")
        expert = self.get_argument('expert')
        glob_select = self.get_argument('select',-1)
        temp_select = glob_select
        if glob_select == '2':
            temp_select = '3'

        elif glob_select== '3':
            temp_select ='2'

        print(glob_select , expert , temp_select)

        if expert == '1':
            print('if')
            videos = Video.select().where(Video.fk_user_expert == user['login_user'], ((Video.status == glob_select)|(Video.status == temp_select)))
            print(videos.count())
        else:
            print('else')
            video_list = SubjectDomain_has_Video.select(SubjectDomain_has_Video.fk_video).where(SubjectDomain_has_Video.fk_expert == user['login_user'])
            videos = Video.select().where(Video.id << video_list , Video.status== glob_select)

        self.render('expert/judged.html', user=user, videos=videos, selected=glob_select , expert =expert )



class Reject_VideoHandler(WebBaseHandler):
    @authentication(14)
    def get(self, *args, **kwargs):
        pass
    @authentication(14)
    def post(self, *args, **kwargs):
        vid = self.get_argument('id')
        comment = self.get_argument('comment',None)
        Video.update(status = -2).where(Video.id == vid).execute()
        session = SessionManager(self)
        sender = session.get('id')
        sender = User.select().where(User.id == sender).get()
        video=Video.select().where(Video.id == vid).get()
        text = "متاسفانه فیلم"  +" " +video.name +" "+"در مرحله کارشناسی اولیه رد شد."

        if comment != None:
            text +="دلیل رد:"
            text+= comment

        send_notification(self,sender,video.fk_uploader,text)
        self.redirect(self.reverse_url('expert_total'))

class Reject_Subject_VideoHandler(WebBaseHandler):
    @authentication(16)
    def get(self, *args, **kwargs):
        pass
    @authentication(16)
    def post(self, *args, **kwargs):
        session = SessionManager(self)
        user_id = session.get('id')
        user = User.select().where(User.id == user_id).get()
        vid= self.get_argument('id')
        messege= self.get_argument('msg')
        if messege:
            video = Video.select().where(Video.id == vid).get()
            comment = "فیلم" +" " + video.name +" " +"ردشد.لطفا مجددا نسبت به کارشناسی اقدام نمایید.علت رد: "  +" "
            comment+= messege
            expert = User.select().where(User.id == video.fk_user_expert).get()
            Video.update(status = 1).where(Video.id == vid).execute()
            SubjectDomain_has_Video.update(fk_expert=None , status = 0).where(SubjectDomain_has_Video.fk_video == video).execute()
            send_notification(self,user,expert,comment)
        self.redirect(self.reverse_url('expert_subject'))

