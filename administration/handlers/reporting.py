# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

import tornado.web

from administration.classes.Public_actions import *
import jdatetime
import calendar
from base.handlers.base import WebBaseHandler


def add_months(sourcedate,months):
     month = sourcedate.month - 1 + months
     year = int(sourcedate.year + month / 12 )
     month = month % 12 + 1
     day = min(sourcedate.day,calendar.monthrange(year,month)[1])
     return datetime.date(year,month,day)

def count_item(obj, start_date, end_date):
    start = jdatetime.JalaliToGregorian(start_date)
    # start = JalaliDate(start_date)
    start = start.todate()

    # end = JalaliDate(end_date)
    end = jdatetime.JalaliToGregorian(end_date)

    end = end.todate()
    count = obj.select().where((obj.inserted_date.between(start,end_date))).count()

    return count



    pass
def count_item_whit_con(obj, start_date, end_date ,inobj):

    # start = JalaliDate(start_date)
    start = jdatetime.JalaliToGregorian(start_date)

    start = start.todate()

    end = jdatetime.JalaliToGregorian(end_date)
    # end = JalaliDate(end_date)
    end = end.todate()

    count = obj.select().where(obj.inserted_date >=start, obj.inserted_date < end , obj.id << inobj).count()
    return count



    pass
def split_by_day(start_date , end_date):
    date_array =[]
    date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
    start_date = start_date + datetime.timedelta(days=1)
    end_date = end_date + datetime.timedelta(days = 0)

    while( end_date > start_date):
        date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
        start_date = start_date  + datetime.timedelta(days = 1)

    date_array.append(datetime.date(end_date.year,end_date.month,end_date.day))
    return date_array
def split_by_week(start_date , end_date):
    date_array =[]
    date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
    start_date = start_date + datetime.timedelta(days=7)
    end_date = end_date + datetime.timedelta(days = 0)
    while( end_date > start_date):
         date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
         start_date = start_date  + datetime.timedelta(days = 7)

    date_array.append(datetime.date(end_date.year,end_date.month,end_date.day))
    return date_array
def split_by_month(start_date , end_date):
    date_array =[]
    date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
    start_date = add_months(start_date , 1)

    while( end_date > start_date):
         date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
         start_date = add_months(start_date , 1)

    date_array.append(datetime.date(end_date.year,end_date.month,end_date.day))
    return date_array
def split_by_year(start_date , end_date):
    date_array =[]
    date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
    start_date = start_date + datetime.timedelta(days=365)
    end_date = end_date + datetime.timedelta(days = 0)

    while( end_date > start_date):
        date_array.append(datetime.date(start_date.year,start_date.month,start_date.day))
        start_date = start_date  + datetime.timedelta(days = 365)

    date_array.append(datetime.date(end_date.year,end_date.month,end_date.day))
    return date_array
def splite_date(type , start , end):
    resualt =[]
    if type== 'y':
        resualt = split_by_year(start,end)
    elif type == 'm':
        resualt = split_by_month(start,end)
    elif type == 'w':
        resualt = split_by_week(start,end)
    else:
        resualt = split_by_day(start,end)
    return resualt
def search(obj,array_date,type):
        temp_dict={}
        resualt ={}
        lenght = len(array_date)
        resualt[0] = lenght
        for i in range(1,lenght):
             count =count_item(obj,array_date[i-1],array_date[i])
             start = jdatetime.JalaliToGregorian(array_date[i-1])
             end = jdatetime.JalaliToGregorian(array_date[i])
             if(type == 'd'):
                 temp_dict[i] = str(count) + ':' + str(start)
             else:
                 temp_dict[i] = str(count) + ':' +  str(start)+' - '+ str(end)

             resualt[i] = temp_dict
        return resualt
def searchinrange(obj,array_date , con,type):
        temp_dict={}
        resualt = {}
        lenght = len(array_date)
        resualt[0] = lenght
        for i in range(1,lenght):
             count =count_item_whit_con(obj,array_date[i-1],array_date[i],con)
             start = jdatetime.JalaliToGregorian(array_date[i-1])
             end = jdatetime.JalaliToGregorian(array_date[i])
             if(type == 'd'):
                 temp_dict[i] = str(count) + ':' + str(start)
             else:
                temp_dict[i] = str(count) + ':' +  str(start)+' - '+ str(end)
             resualt[i] = temp_dict
        return resualt



class ReportingHandler(WebBaseHandler):
    @authentication(34)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد","گزارش گیری","گزارش گیری")
        self.render('report/report.html', user=user)

    @authentication(34)
    def post(self, *args, **kwargs):
        obj = self.get_argument('obj')
        type = self.get_argument('type')
        date = self.get_argument('date')
        start = self.get_argument('start')
        end = self.get_argument('end')
        temp = start.split('/')
        start_date = jdatetime.JalaliToGregorian(int(temp[0]) , int(temp[1]) , int(temp[2]))
        temp = end.split('/')
        end_date = jdatetime.JalaliToGregorian(int(temp[0]) , int(temp[1]) , int(temp[2]))

        start_date = jdatetime.JalaliToGregorian(start_date).todate()
        end_date = jdatetime.JalaliToGregorian(end_date).todate()
        if start_date == end_date :
            end_date = end_date  + datetime.timedelta(days = 1)
        array_date = splite_date(date,start_date,end_date)

        if obj=='user':
            resualt = search(User,array_date,date)

        elif obj=='heyat':
            if type == 'all':
                resualt = search(Heyat,array_date,date)

            elif type== 'accepted':
                heyats = Heyat.select().where(Heyat.is_active == 1)
                resualt = searchinrange(Heyat,array_date,heyats,date)

            elif type=='rejected':
                heyats = Heyat.select().where(Heyat.is_active == -1)
                resualt = searchinrange(Heyat,array_date,heyats,date)
        else:
            if type == 'all':
                resualt = search(Video,array_date,date)

            elif type== 'accepted':
                videos = Video.select().where(Video.status == 3)
                resualt = searchinrange(Video,array_date,videos,date)

            elif type=='rejected':
                videos = Video.select().where(Video.status == 0)
                resualt = searchinrange(Video,array_date,videos,date)

        self.write(resualt)


