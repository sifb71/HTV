# -*- coding: UTF-8 -*-

import re
import os
import tornado.web
import tornado.httputil
from base.handlers.base import WebBaseHandler
from config import Config
import os.path
import random
# import Image
from PIL import Image
from administration.classes.Public_actions import *

__author__ = 'Iman'

def type_of_file(name):  # get file type from file_name
    name = name.split(".")[-1]
    return name

def create_name(user_id):
        u = User.select().where(User.id == user_id).get()
        name = u.fore_name
        name += "-" + u.sur_name + str(random.randint(10000,99999))
        return name



# class BaseHandler(WebBaseHandler):
#     def __init__(self, application, request, **kwargs):
#         super(BaseHandler, self).__init__(application, request, **kwargs)
#
#
# class MediaBaseHandler(BaseHandler):
#     def __init__(self, application, request, **kwargs):
#         super(MediaBaseHandler, self).__init__(application, request, **kwargs)

class StreamingPicHandler(WebBaseHandler):

    def initialize(self):
        self.sess = SessionManager(self)



    @authentication(24)
    def get(self, *args, **kwargs):
        user = get_curent_info(self,"داشبورد",  "مدیریت ویدئو ها",  "آپلود ویدئو - گام دوم")


        self.render("video_and_pic.html",user=user)
    @authentication(24)
    def post(self, *args, **kwargs):
        path = str(self.sess.get('heyat'))
        upload_directory=os.path.join(Config().upload_pic,path)
        if not os.path.exists(upload_directory):
            os.mkdir(upload_directory)
        perifix = ['jpg' , 'jpeg' ,'png']
        filee = self.request.files['file'][0]
        f = filee['filename']
        per = f.split('.')
        per = per[len(per) - 1].lower()
        if not per in perifix:
            self.write('error')
            return
        file_name = create_name(self.sess.get('id'))
        output = open(upload_directory + '/' + file_name, 'wb')
        output.write(filee['body'])
        output.close()
        self.sess.set("temp_pic_name" , file_name)
        size= 700,700
        im = Image.open(upload_directory + '/' + file_name)
        im.thumbnail(size, Image.ANTIALIAS)
        im.save(upload_directory + '/' + file_name, "JPEG")
        self.write('true')


