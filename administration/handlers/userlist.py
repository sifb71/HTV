# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad hefazi,s.i.feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class UserListHandler(WebBaseHandler):
    @authentication(11)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد" ,"مدیریت کاربران  " , "لیست کاربران" )
        session = SessionManager(self)
        user_id = session.get('id')
        user_role_id = session.get('role')
        heyat_id = session.get('heyat')
        user_role = Role().select().where(Role.id == user_role_id).get()
        role_level = user_role.level
        users = None
        fname = self.get_argument('fname',"")
        lname = self.get_argument('lname',"")
        email = self.get_argument('email',"")
        phone = self.get_argument('phone',"")
        sort_by = self.get_argument('sort',"sur_name")
        sort_type = self.get_argument('type',"A")
        page = int(self.get_argument('page', 1))

        if page < 0:
            page = 1



        if role_level == 1:  # if curent user's level is Kol
            print('l1')
            # select all other user
            if sort_type == "A":
                temp_role = Role.select().where(Role.id == 13).get()
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                    ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                    .order_by(SQL(sort_by).desc()).paginate(page, 20)

                sort_type = 'D'

            else:
                temp_role = Role.select().where(Role.id == 13).get()
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") )\
                .order_by(SQL(sort_by).asc()).paginate(page, 20)
                sort_type = 'A'
            count = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                ,User.email**("%"+ email+"%"), User.phone**("%"+ phone +"%") ).count()

        elif role_level == 2:  # if curent user's level is Province


            if user['login_user'].fk_address is not None:
                # select all user in my province
                address = Address().select().where(Address.province == user['login_user'].fk_address.province)
                temp_role = Role.select().where(Role.id == 13).get()

                if sort_type == "A":

                    users = User().select() \
                      .where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.fk_address << address, User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                      ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                      .order_by(SQL(sort_by).desc()).paginate(page, 20)


                    sort_type = "D"

                else:

                    users = User().select() \
                    .where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.fk_address << address, User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                    ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                    .order_by(SQL(sort_by).asc()).paginate(page, 20)
                    sort_type = "A"
                count = users = User().select() \
                    .where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.fk_address << address, User.id != user_id, User.is_deleted != 1, User.fk_role != temp_role\
                    ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") ).count()


            else:
                print('l2 else')
                print('user do not have address')
                self.write('user do not have address')

        else:  # if curent user's level is Heyat
            print('l3')
            heyat = Heyat().select().where(Heyat.id == heyat_id)
            heyat_member = Heyat_has_user().select(Heyat_has_user.fk_user) \
                .where(Heyat_has_user.fk_heyat == heyat,Heyat_has_user.is_accept == 1)
            # select all user who are member in my Heyat
            temp_role = Role.select().where(Role.id == 13).get()
            if sort_type == "A":
                users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.is_deleted != 1 , User.fk_role != temp_role \
                ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                .order_by(SQL(sort_by).desc()).paginate(page, 20)
                sort_type = "D"

            else:

                 users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.is_deleted != 1 , User.fk_role != temp_role \
                  ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
                  .order_by(SQL(sort_by).asc()).paginate(page, 20)
                 sort_type = "A"

            count = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.is_deleted != 1 , User.fk_role != temp_role \
                  ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") ).count()

        filter=[fname , lname ,email,phone,sort_by,sort_type]
        self.render('users/users.html', user=user, users=users , page=page , count=count, filter=filter)

class Request_UserListHandler(WebBaseHandler):
    @authentication(33)
    def get(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد" ,"مدیریت کاربران  " , "درخواست های جدید" )
        session = SessionManager(self)
        user_id = session.get('id')
        user_role_id = session.get('role')
        heyat_id = session.get('heyat')
        user_role = Role().select().where(Role.id == user_role_id).get()
        role_level = user_role.level
        users = None
        fname = self.get_argument('fname',"")
        lname = self.get_argument('lname',"")
        email = self.get_argument('email',"")
        phone = self.get_argument('phone',"")
        sort_by = self.get_argument('sort',"sur_name")
        sort_type = self.get_argument('type',"A")
        page = int(self.get_argument('page', 1))
        print(heyat_id)
        print(sort_type)

        if page < 0:
            page = 1

        heyat = Heyat().select().where(Heyat.id == heyat_id)
        heyat_member = Heyat_has_user().select(Heyat_has_user.fk_user) \
            .where(Heyat_has_user.fk_heyat == heyat,Heyat_has_user.is_accept == 0)
        # select all user who are member in my Heyat
        temp_role = Role.select().where(Role.id == 13).get()
        if sort_type == "A":
            users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member , User.fk_role != temp_role \
            ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
            .order_by(SQL(sort_by).desc()).paginate(page, 20)
            sort_type = "D"

        else:

             users = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member, User.fk_role != temp_role \
              ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") )\
              .order_by(SQL(sort_by).asc()).paginate(page, 20)
             sort_type = "A"

        count = User().select().where(User.fore_name**("%"+fname+"%"),User.sur_name**("%"+lname+"%"),User.id << heyat_member , User.fk_role != temp_role \
              ,User.email **("%" + email + "%"), User.phone** ("%" + phone + "%") ).count()

        filter=[fname , lname ,email,phone,sort_by,sort_type]
        self.render('users/request_users.html', user=user, users=users , page=page , count=count, filter=filter)
    @authentication(33)
    def post(self, *args, **kwargs):
        session = SessionManager(self)
        login_user_id = session.get('id')

        user_request_id = self.get_argument('id')
        heyat_id = session.get('heyat')
        user_request = User.select().where(User.id== user_request_id).get()
        heyat = Heyat.select().where(Heyat.id == heyat_id).get()
        Heyat_has_user.update(is_accept=1).where(Heyat_has_user.fk_heyat == heyat , Heyat_has_user.fk_user == user_request).execute()
        request = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat , Heyat_has_user.fk_user == user_request).get()
        user = request.fk_user
        msg = "در خواست شما برای عضویت در هیئت" + " "+ request.fk_heyat.name +" " +"با موفقیت تایید شده"
        send_notification(self,login_user_id,user,msg)
        self.redirect(self.reverse_url('request_member'))

class UserActiveHandler(WebBaseHandler):
    @authentication(33)
    def get(self, *args, **kwargs):
        pass
    @authentication(33)
    def post(self, *args, **kwargs):
        user_id = self.get_argument('id')
        User.update(is_active=1).where(User.id == user_id).execute()
        self.redirect(self.reverse_url('user_list'))
