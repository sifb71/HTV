#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################


import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class SetRoleHandler(WebBaseHandler):
        @authentication(1)
        def get(self, *args, **kwargs):
            user = get_curent_info(self ,"داشبورد" , "مدیریت نقش های سیستم",  "مدیریت نقش های کاربران")
            self.render('roles/modify_roles.html', user=user)
        @authentication(1)
        def post(self, *args, **kwargs):
           pass


class ReloadHandler(WebBaseHandler):
    @authentication(1)
    def get(self, *args, **kwargs):
        action = Action().select()
        subaction = Sub_Action().select()
        roles = Role().select().where(Role.id != 1 , Role.id != 13)
        showaction = Role().select().where(Role.id != 1).get()
        role_action = Role_Action().select().where(Role_Action.fk_role == showaction)
        array = []
        for i in role_action:
            array.append(i.fk_sub_action.id)
        self.render('roles/set_role.html', roles=roles, action=action,
                    subaction=subaction, role_action=array, showaction=showaction.id)
    @authentication(1)
    def post(self, *args, **kwargs):
        role_id = self.get_argument('action')
        print(role_id)
        action = Action().select()
        subaction = Sub_Action().select()
        roles = Role().select().where(Role.id != 1 , Role.id != 13)
        get_role = Role().select().where(Role.id == role_id).get()
        role_action = Role_Action().select().where(Role_Action.fk_role == get_role)
        array = []
        for i in role_action:
            array.append(i.fk_sub_action.id)
        self.render('roles/set_role.html', roles=roles, action=action,
                    subaction=subaction, role_action=array, showaction=role_id)


class SaveActionHandler(WebBaseHandler):
    @authentication(2)
    def get(self, *args, **kwargs):
        pass
    @authentication(2)
    def post(self, *args, **kwargs):
        role_id = self.get_argument('role')
        role = Role().select().where(Role.id == role_id).get()

        if (role.id == 1):
            self.write('مجاز به تغییر دسترسی مدیر نیستید')
        else:
            Role_Action().delete().where(Role_Action.fk_role == role).execute()

            sub_actions = Sub_Action().select()
            for i in sub_actions:
                if (self.get_arguments(str(i.id))):
                    sub_action = Sub_Action().select().where(Sub_Action.id == i.id).get()
                    Role_Action().create(fk_role=role, fk_sub_action=sub_action)
            self.redirect(self.reverse_url('roles'))


class AddRoleHandler(WebBaseHandler):
    @authentication(2)
    def get(self, *args, **kwargs):

        user = get_curent_info(self,"داشبورد" ,"مدیریت نقش های سیستم" , "تعریف نقش های جدید" )
        private_role =[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        role = Role().select().where(~(Role.id << private_role))
        self.render('roles/modify_add_role.html', user=user, role=role)
    @authentication(2)
    def post(self, *args, **kwargs):
        role_name = self.get_argument('new_role')
        level = self.get_argument('level')
        Role().create(name=role_name, level=level)
        self.redirect(self.reverse_url('add_role'))


class DeleteRoleHandler(WebBaseHandler):
    @authentication(2)
    def get(self, *args, **kwargs):
        pass
    @authentication(2)
    def post(self, *args, **kwargs):
        try:
            role_id = self.get_argument('id')
            role = Role().select().where(Role.id == role_id).get()

            Role_Action.delete().where(Role_Action.fk_role == role).execute()
            public_role = Role.select().where(Role.id == 10).get()
            User.update(fk_role = public_role).where(User.fk_role == role).execute()
            Role().delete().where(Role.id == role_id).execute()

            self.redirect(self.reverse_url('add_role'))

        except:
            self.write('one error occure!')




