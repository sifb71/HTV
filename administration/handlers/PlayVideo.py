# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from heyat.classes.video_keywords import *
from base.classes.VideoScore import *
from administration.classes.Public_actions import *
from pycket.session import SessionManager
from multiprocessing import Lock

class play_VideoHandler(WebBaseHandler):
    @authentication(16)
    def get(self, *args, **kwargs):
        pass

    @authentication(16)
    def post(self, *args, **kwargs):
        user = get_curent_info(self, "داشبورد", "کارشناسی", "دادن امتیاز به ویدئو")
        vid = self.get_argument('videoid')
        sub = self.get_argument('sub')
        video = Video.select().where(Video.id == vid).get()
        subject = Subject_Domain.select().where(Subject_Domain.id == sub).get()
        sub_domains = Sub_Domain.select().where(Sub_Domain.fk_subject_domain == subject)

        key_words = Video_Keyword.select(Video_Keyword.fk_keyword).where(Video_Keyword.fk_video == video)
        artists = Artist_has_Video.select(Artist_has_Video.fk_user).where(Artist_has_Video.fk_video == video)

        temp_artist = ''
        for item in artists:
            temp_artist += ',' + item.fk_user.fore_name + ' ' + item.fk_user.sur_name + ','

        temp_key = ''
        for item in key_words:
            temp_key += ',' + item.fk_keyword.name + ','
        self.render('play_video_for_expert2.html', user=user, video=video, sdomain=sub_domains, keywords=key_words,
                    artists=artists, hartist=temp_artist, hkeyword=temp_key, sname=subject)


class Accept_Video_Scoring(WebBaseHandler):
    @authentication(16)
    def get(self, *args, **kwargs):
        pass
    @authentication(16)
    def post(self, *args, **kwargs):
        print('sssssssssssss')
        session = SessionManager(self)
        id = session.get('id')
        user = User.select().where(User.id == id).get()
        video_id = self.get_argument('id')
        sub = self.get_argument('sdomainid')
        print(sub)
        name = self.get_argument('name')
        headline = self.get_argument('name')
        headline_main = self.get_argument('name')
        comment = self.get_argument('name')
        upload_date = self.get_argument('date')
        artists = self.get_argument('artists')
        keywords = self.get_argument('keywords')
        video = Video.select().where(Video.id == video_id).get()

        if name and headline and headline_main and upload_date:
            if video.fk_address != None:
                province = self.get_argument('ostan').strip()
                city = self.get_argument('city').strip()
                village = self.get_argument('village').strip()
                address = self.get_argument('address').strip()
                fk_address = Address.update(province=province, city=city, village=village, address=address).where(Address.id == video.fk_addres).execute()
            else:
                fk_address = None

            mutex = Lock()
            with mutex:

                Video_Keyword.delete().where(Video_Keyword.fk_video == video).execute()
                Artist_has_Video.delete().where(Artist_has_Video.fk_video == video).execute()
                add_keyword_to_video(keywords, video)
                add_artist_to_video(artists, video)

                domain = Subject_Domain.select().where(Subject_Domain.id == sub).get()
                sub_domain = Sub_Domain.select().where(Sub_Domain.fk_subject_domain == domain)

                for item in sub_domain:
                    score = self.get_argument(str(item.id)).strip()
                    print(score)
                    if score == None or score =="":
                        score = 0
                    print(item.id , video.id , score)
                    Score_has_Video.create(fk_sub_domain=item, fk_video=video, score=score)

                score = cal_video_scoer(video)

                is_accept = 2
                SubjectDomain_has_Video.update(status = 1).where(SubjectDomain_has_Video.fk_subject_domain == domain , SubjectDomain_has_Video.fk_video==video).execute()
                print(video.id , domain.id , user.id)
                status = SubjectDomain_has_Video.select()\
                    .where(SubjectDomain_has_Video.fk_video == video , SubjectDomain_has_Video.fk_subject_domain == domain ,SubjectDomain_has_Video.fk_expert ==user, SubjectDomain_has_Video.status ==0).count()
                print(status)

                if status == 0:
                    is_accept = 3


                Video.update(name=name, headline=headline, headline_main=headline_main, comment=comment \
                             , date=upload_date, fk_address=fk_address, score=score , status=is_accept).where(
                    Video.id == video_id).execute()
                self.redirect(self.reverse_url('expert_subject'))


class Expert_one_Play_Video(WebBaseHandler):
    @authentication(14)
    def get(self, *args, **kwargs):
       pass
    @authentication(14)
    def post(self, *args, **kwargs):
        print('poset')
        user = get_curent_info(self,"داشبورد","مدیریت ویدئوها","پخش ویدئو")
        video_id = self.get_argument('id')
        print(video_id)
        video = Video.select().where(Video.id == video_id).get()
        key_words = Video_Keyword.select(Video_Keyword.fk_keyword).where(Video_Keyword.fk_video == video)
        artists = Artist_has_Video.select(Artist_has_Video.fk_user).where(Artist_has_Video.fk_video == video)

        temp_artist = ''
        for item in artists:
            temp_artist += ',' + item.fk_user.fore_name + ' ' + item.fk_user.sur_name + ','


        temp_key = ''
        for item in key_words:
            temp_key += ',' + item.fk_keyword.name + ','


        subject_video = Subject_Domain.select().where(Subject_Domain.id != 9)

        self.render('play_video_for_expert1.html', user=user, video=video, keywords=key_words,
                    artists=artists, hartist=temp_artist, hkeyword=temp_key,subject =subject_video)

class Expert_one_Accept_Video(WebBaseHandler):
    @authentication(14)
    def get(self, *args, **kwargs):
        pass
    @authentication(14)
    def post(self, *args, **kwargs):

        video_id = self.get_argument('id')
        name = self.get_argument('name')
        headline = self.get_argument('headline')
        headline_main = self.get_argument('headline_main')
        comment = self.get_argument('comment')
        upload_date = self.get_argument('date')
        artists = self.get_argument('artists')
        keywords = self.get_argument('keywords')
        domains = self.get_argument('subjects')

        flag_address = 0

        video = Video.select().where(Video.id == video_id).get()

        if name and headline and headline_main and upload_date and domains:
            if video.fk_address != None:
                province = self.get_argument('ostan').strip()
                city = self.get_argument('city').strip()
                village = self.get_argument('village').strip()
                address = self.get_argument('address').strip()
                fk_address = video.fk_address
                Address.update(province=province, city=city, village=village, address=address).where(Address.id == fk_address).execute()

            Video_Keyword.delete().where(Video_Keyword.fk_video == video).execute()
            Artist_has_Video.delete().where(Artist_has_Video.fk_video == video).execute()



            if ~add_keyword_to_video(keywords, video) | ~add_artist_to_video(artists, video):
                self.write('one error occre!')




            num_subject_domain = add_subject_domain_to_video(domains,video)
            if num_subject_domain is None:
                self.write('one error occre!')


            if flag_address == 1:
                Video.update(name=name, headline=headline, headline_main=headline_main, comment=comment\
                             , upload_date=upload_date, fk_address=fk_address,  status=2)\
                    .where(Video.id == video_id).execute()
            else:
                Video.update(name=name, headline=headline, headline_main=headline_main, comment=comment\
                             , date=upload_date,  status=2)\
                     .where(Video.id == video_id).execute()


            self.redirect(self.reverse_url('expert_total'))
        self.write('one error occre!')

class Public_video_play(WebBaseHandler):
    @authentication(25)
    def get(self, *args, **kwargs):
        pass
    @authentication(25)
    def post(self, *args, **kwargs):

        user =get_curent_info(self,"داشبورد","فیلم ها","پخش فیلم")
        vid = self.get_argument('id')
        video = Video.select().where(Video.id == vid).get()
        keywords = Video_Keyword.select().where(Video_Keyword.fk_video == video)
        artists = Artist_has_Video.select().where(Artist_has_Video.fk_video == video)
        subjects = SubjectDomain_has_Video.select().where(SubjectDomain_has_Video.fk_video == video)
        comment = Video_Has_Comment.select().where(Video_Has_Comment.fk_video==video, Video_Has_Comment.status== 0)
        self.render('public_play_video.html',user=user, video=video, keywords=keywords, artists=artists, subjects=subjects, comment=comment)


