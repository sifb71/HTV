#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class SD_ScoringCoeffHandler(WebBaseHandler):
    @authentication(3)
    def get(self, *args, **kwargs):

        user = get_curent_info(self,"داشبورد" ,"مدیریت نقش های سیستم" , "ضرایب امتیاز دهی" )
        domain = Subject_Domain().select().where(Subject_Domain.id != 9)
        sub_domain = Sub_Domain().select()

        self.render('expert/domain_score.html' ,user=user, domain=domain, sub_domain=sub_domain)
    @authentication(3)
    def post(self, *args, **kwargs):
        sub_domain = Sub_Domain().select().where(Sub_Domain.id != 25)
        for item in sub_domain:

            try:
                domain_rate = self.get_argument(str(item.id))
                Sub_Domain().update(ratio = domain_rate).where(Sub_Domain.id == item.id).execute()

            except:
                self.write('Ratio Is UnValid')

        self.redirect(self.reverse_url('domain_score'))

class Add_Sub_DomainHandler(WebBaseHandler):
    @authentication(4)
    def get(self, *args, **kwargs):
        pass
    @authentication(4)
    def post(self, *args, **kwargs):
        sub_domain_name = self.get_argument('sub_domain').strip()

        if sub_domain_name != "":
            domain_id = self.get_argument('domain')

            try:
                domain = Subject_Domain().select().where(Subject_Domain.id == domain_id)
                Sub_Domain().create(name = sub_domain_name, fk_subject_domain = domain).execute()
            except:
                self.write('one error occure!')

        self.redirect(self.reverse_url('domain_score'))

class Delete_DomainHandler(WebBaseHandler):
    @authentication(4)
    def get(self, *args, **kwargs):
        pass
    @authentication(4)
    def post(self, *args, **kwargs):
        domain_id = self.get_argument('id')
        sub_domain = Sub_Domain.select().where(Sub_Domain.id == domain_id).get()
        print(domain_id)
        try:
            Score_has_Video().delete().where(Score_has_Video.fk_sub_domain == sub_domain).execute()
            Sub_Domain().delete().where(Sub_Domain.id == domain_id).execute()
        except:
            self.write('one error occure!')











