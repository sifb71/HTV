# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
###############################################################################
import functools

from pycket.session import SessionManager

from base.models.base_model import *
from heyat.handlers.websocket import EchoWebSocket


def get_curent_info(self,title,page_name, sub_page_name):
    current_user = {}
    session = SessionManager(self)
    print(session.get('fname'))
    current_user['name'] = session.get('fname') + " " +session.get('lname')
    login_user = User.select().where(User.id == int(session.get('id'))).get()
    role = session.get('role')
    current_user['notification'] =  Notification.select().where(Notification.fk_receiver == login_user )\
        .order_by(Notification.inserted_date.desc()).limit(5)

    new_notification = Notification.select().where(Notification.is_read == 0, Notification.fk_receiver == login_user)
    current_user['count_notification'] = new_notification.count()
    current_user['sub_action'] = Role_Action.select().where(Role_Action.fk_role == role)
    sub_action_for_action = Role_Action.select(Role_Action.fk_sub_action).where(Role_Action.fk_role == role)
    action = Sub_Action.select(Sub_Action.fk_action)\
        .where(Sub_Action.id << sub_action_for_action)\
        .group_by(Sub_Action.fk_action)

    current_user['action'] = action
    current_user['login_user'] = login_user
    current_user['new_notification'] = new_notification
    current_user['title'] = title
    current_user['page_name'] = page_name
    current_user['page_name2'] = sub_page_name
    return current_user

def send_notification(self,sender , receiver , text):
    try:
        Notification.create(fk_receiver=receiver, fk_sender=sender, text=text)
        return True
    except:
        print('error')
        return False

def send_Nofification_to_super_admin(sender,text):
    role = Role.select().where(Role.id == 1).get()
    admins = User.select().where(User.fk_role == role)
    try:
        for item in admins:
            Notification.create(fk_receiver=item, fk_sender=sender, text=text)

        return True

    except:
        return False

def authentication(sub_id):
    sub_id = sub_id if sub_id else 0

    def f(func):
        @functools.wraps(func)
        def func_wrapper(self, *args, **kwargs):
            session = SessionManager(self)
            login_id = session.get('id')
            if login_id == None :
                from config import Config
                pubic_url = Config().packages['PUBLIC_SYSTEM']
                self.redirect(pubic_url + '/login')
                return

            try:
                print(session.get('role'))
                Role_Action.select().where(Role_Action.fk_role == session.get('role'), Role_Action.fk_sub_action == sub_id ).get()
                pass
            except:
                from config import Config
                pubic_url = Config().packages['PUBLIC_SYSTEM']
                self.redirect(pubic_url + '/error/404')
                return

            return func(self, args, *kwargs)

        return func_wrapper

    return f