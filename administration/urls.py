# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

from administration.handlers.heyats import *
from administration.handlers.SD_DomainExperts import *
from administration.handlers.RM_RoleManagement import *
from administration.handlers.SD_ScoringCoeff import *
from administration.handlers.UM_AddNewUser import Add_UserHandler
from administration.handlers.Admin_dashboard import *
from administration.handlers.userlist import *
from administration.handlers.add_user_role import *
from administration.handlers.uploaded_videos import *
from administration.handlers.Judgement import *
from administration.handlers.upload_video import StreamingHandler,VideoUploadFinishHandler
from administration.handlers.upload_pics import StreamingPicHandler
from administration.handlers.video_upload import *
from administration.handlers.PlayVideo import *
from administration.handlers.user_create_heyat import *
from administration.handlers.user_heyat_request import *
from administration.handlers.reporting import *
from administration.handlers.comment import *
from administration.handlers.messege import *
url_patterns = [
    (r'/administration/dashboard', DashboardHandler, None, 'dashboard'),
    (r'/administration/roles/modify', SetRoleHandler, None, 'roles'),
    (r'/administration/roles/modify/reload', ReloadHandler, None, 'reload'),
    (r'/administration/add_role', AddRoleHandler, None, 'add_role'),
    (r'/administration/delete_role', DeleteRoleHandler, None, 'delete_role'),
    (r'/administration/heyat/change_level', HM_ModifyHeyatLevelHandler, None, 'change_level'),
    (r'/administration/uploaded_videos', UploadedListHandler, None, 'uploaded_videos'),
    (r'/administration/subject_videos', SubjectiveJudgementHandler, None, 'subject_videos'),
    (r'/administration/select/subject_videos', SubjectiveJudgementHandler_, None, 'select_subject'),
    (r'/administration/experts/total', TotalJudgementHandler, None, 'expert_total'),
    (r'/administration/experts/subject', SubjectJudgementHandler, None, 'expert_subject'),
    (r'/administration/experts/Judged', JudgedHandler, None, 'Judged'),
    (r'/administration/domain_list', DomainListHandler, None, 'domain_list'),
    (r'/administration/add_expert', Add_New_Expert, None, 'add_expert'),
    (r'/administration/delete_expert', Delete_ExpertHandler, None, 'delete_expert'),
    (r'/administration/domain_score', SD_ScoringCoeffHandler, None, 'domain_score'),
    (r'/administration/add_domain', Add_Sub_DomainHandler, None, 'add_domain'),
    (r'/administration/delete_domain', Delete_DomainHandler, None, 'delete_domain'),
    (r'/administration/heyat/modify', HeyatModifyHandler, None, 'heyat_modify'),
    (r'/admin/active_heyat', AcceptHeyatHandler, None, 'active_heyat'),
    (r'/admin/delete_heyat', DeleteHeyatHandler, None, 'delete_heyat'),
    (r'/administration/add_user', Add_UserHandler, None, 'add_new_user'),
    (r'/administration/user_list', UserListHandler, None, 'user_list'),
    (r'/administration/add_user_role', AddUserRoleHandler, None, 'add_user_role'),
    (r'/administration/change_acton', SaveActionHandler, None, 'change_action'),
    (r'/admin/upload', StreamingHandler, None, 'upload_video'),
    (r'/admin/upload/pic', StreamingPicHandler, None, 'upload_pic'),
    (r'/admin/video/upload_step1', VideoUploadHandler, None, 'video_upload_1'),
    (r'/admin/video/finish', VideoUploadFinishHandler, None, 'video_upload_finish'),
    (r'/admin/artist/auto_load',Artist_AutoLoadHandler, None, 'artists_load'),
    (r'/admin/keyword/auto_load',Keyword_AoutoLoadHandler, None, 'keywords_load'),
    (r'/admin/videos/sub_expert/play', play_VideoHandler, None, 'play_video_for_subject_expert'),
    (r'/videos/save_score', Accept_Video_Scoring, None, 'save_diveo_score'),
    (r'/user/create_heyat', CreateHeyatHandler, None, 'usercreatheyat'),
    (r'/administration/deactive_heyats', AcceptHeyatHandler, None, 'deactive_heyat_list'),
    (r'/administration/delete_heyat_request', DeleteHeyatHandler, None, 'dont_accept_heay_request'),
    (r'/admin/finish_create_heyat', FinishCreateHeyatHandler, None, 'finish_create_heyat'),
    (r'/admin/heyat/error', CreateHeyat_Error_Handler, None, 'error_create_heyat'),
    (r'/admin/myuploaded', MyUploadedVideosHandler, None, 'my_uploaded_videos'),
    (r'/administration/video/expert/play', Expert_one_Play_Video, None, 'expert_one_video_play'),
    (r'/administration/experts/total/reject', Reject_VideoHandler, None, 'reject_vedio_exprt'),
    (r'/administration/experts/accept', Expert_one_Accept_Video, None, 'expert_one_accept'),
    (r'/admin/video/play', Public_video_play,None,'public_video_play'),
    (r'/admin/expert/reject_subject', Reject_Subject_VideoHandler,None,'reject_video_subject'),
    (r'/user/heyat_request/member', HeyatRequestHandler, None, 'member_request_heyat'),
    (r'/user/heyat/search', HeyatSearchRequestHandler, None, 'request_heyat_search'),
    (r'/admin/heyat/new_request/reject', HeyatNewRequestRejectHandler, None, 'my_heyat_request_reject'),
    (r'/administration/report', ReportingHandler, None, 'report'),
    (r'/administration/edit_info', Edit_infoHandler, None, 'user_edit_profile'),
    (r'/administration/change_heyat_logo', Edit_Heyat_logoHandler, None, 'heyat_edit_logo'),
    (r'/administration/change_pass', change_passHandler, None, 'user_change_pass'),
    (r'/admin/heyat/auto_load',Heyat_AoutoLoadHandler, None, 'heyat_load'),
    (r'/admin/user/auto_load',User_AutoLoadHandler, None, 'user_load'),
    (r'/admin/video/acceptcomment',Accept_CommentHandler, None, 'accept_comment'),
    (r'/admin/video/delete_comment',Delete_CommentHandler, None, 'delete_comment'),
    (r'/admin/inbox',Inbox_MessegeHandler, None, 'message_inbox'),
    (r'/admin/inbox/delete',Delete_MessegeHandler, None, 'delete_message'),
    (r'/admin/member_request',Request_UserListHandler, None, 'request_member'),
    (r'/admin/upload_permission',Upload_PermissionHandler, None, 'user_upload_permision'),
    (r'/admin/upload_permission/add',add_Upload_PermissionHandler, None, 'add_user_upload_permision'),
    (r'/admin/upload_permission/list',Uploaded_Permosin_users, None, 'list_user_upload_permision'),
    (r'/administration/users/active',UserActiveHandler, None, 'user_list_active_user'),
    (r'/admin/update/heyat',Upload_Heyat_info, None, 'update_heyat'),














]