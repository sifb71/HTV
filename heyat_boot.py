
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: describe the general purpose of the module
#                as concisely as possible
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options
import tornado
import os
from heyat.urls import url_patterns
from tornado.options import options, define
from config import Config
#  from PortalSystem.ui_module import base_ui_module

sh_connection = Config()
define("port", default=sh_connection.heyat['port'], help="run on the given port", type=int)


class HeyatSystemApplication(tornado.web.Application):
    def __init__(self):
        handlers = url_patterns
        settings = dict(
            debug=True,
            autoreload=True,
            cookie_secret=sh_connection.base['cookie_secret'],
            xsrf_cookies=False,
            # login_url=sh_connection.AdminSystem['login_url'],
            # logout_url=sh_connection.AdminSystem['logout_url'],
            # packages=sh_connection.packages,
            template_path=os.path.join(os.path.dirname(__file__), "heyat/template"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            # #ui_modules=base_ui_module,
            **{
                'pycket': {
                    'engine': 'redis',
                    'storage': {
                        'host': sh_connection.base['redis']['host'],
                        'port': sh_connection.base['redis']['port'],
                        'password': sh_connection.base['redis']['password'],
                        'db_sessions': sh_connection.base['redis']['db_sessions'],
                        'db_notifications': sh_connection.base['redis']['db_notifications'],
                        'max_connections': 2 ** 31,
                    },
                    'cookies': {
                        'expires_days': 120,
                        'domain': sh_connection.domain,
                    },
                },
            }
        )
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(HeyatSystemApplication())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()