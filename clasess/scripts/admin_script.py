import socket
from config import Config


sh_connection = Config()

root = sh_connection.applications_root

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# check running admin_boot
result = sock.connect_ex(('127.0.0.1', 8096))
if result == 0:
    pass
else:
    exec(open(root + "admin_boot.py").read(), globals())

