#!/usr/bin/env python
# -*- coding: utf-8 -*-

from crontab import CronTab

cron = CronTab()

cron.remove_all(comment='user_reminder')
job = cron.new('python3 /var/www/HTV/clasess/scripts/admin_script.py', comment="admin")
job.minute.every(2)
job = cron.new('python3 /var/www/HTV/clasess/scripts/base_script.py', comment="base")
job.minute.every(2)
job = cron.new('python3 /var/www/HTV/clasess/scripts/expert_script.py', comment="expert")
job.minute.every(2)
job = cron.new('python3 /var/www/HTV/clasess/scripts/heyat_script.py', comment="heyat")
job.minute.every(2)
try:
    if job.is_valid():
        print('ok')
except:
    pass
