import os
import socket
root = os.path.join(os.path.dirname(__file__), "")


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# check running expert_boot
result = sock.connect_ex(('127.0.0.1', 8098))
if result == 0:
    pass
else:
    exec(open(root + "expert_boot.py").read(), globals())
