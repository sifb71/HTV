
###############################################################################
#
#    AUTHOR: f_shariatmadari
#    PROJECT: heyat_tv
#
#    DESCRIPTION: listen to the 'wait_for_convert' queue and consume its message
#                 whenever pushed in it
#
###############################################################################

from base.classes.queue_handler import QueueHandler


queue = QueueHandler('convert_to_webm', 'webm')
# queue.push('D:/H/RayanSamane/Heyat TV/Source/heyat-tv/wait_for_convert,f_f_4,mp4')
queue.pop()
