# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################


from heyat.handlers.channels import ChannelsHandler, HeyatChannelHandler, HeyatVideoHandler, Channel_LoadingHandler, \
    Heyat_AoutoLoadingHandler
from heyat.handlers.users import MaddahHandler, SokhanranHandler, ProfileHandler, Maddah_AutoLoadHandler, \
    Maddah_LoadingPageHandler, Sokhanran_AutoLoadHandler, Sokhanran_LoadingPageHandler, ShaerHandler, \
    Shaer_AutoLoadHandler, Shaer_LoadingPageHandler
from heyat.handlers.websocket import *
from heyat.handlers.request import HeyatNewRequestHandler, SendMessageHandler
from heyat.handlers.action import SendCommentHandler
from heyat.handlers.error import Error404Handler
from heyat.handlers.scores import ScoreHeyatHandler, ScoreVideoHandler, ScoreuserHandler

from heyat.handlers.download import DownloadFileHandler
from heyat.handlers.captcha import CaptchaHandler

url_patterns = [

    (r'/heyat/channels', ChannelsHandler, None, 'channels'),
    (r'/heyat/channel/(.*)', HeyatChannelHandler, None, 'channel'),
    (r'/heyat/video/(.*)', HeyatVideoHandler, None, 'video'),
    (r'/heyat/user/(.*)', ProfileHandler, None, 'profile'),

    (r'/channel/loading', Channel_LoadingHandler, None, 'channel_loading'),
    (r'/admin/heyat/auto_load', Heyat_AoutoLoadingHandler, None, 'channel_auto_loading'),

    (r'/heyat/users/maddah', MaddahHandler, None, 'maddah_list'),
    (r'/maddah/loading', Maddah_LoadingPageHandler, None, 'maddah_loading'),
    (r'/admin/maddah/auto_load', Maddah_AutoLoadHandler, None, 'maddah_auto_complete'),

    (r'/heyat/sokhanran', SokhanranHandler, None, 'sokhanran_list'),
    (r'/sokhanran/loading', Sokhanran_LoadingPageHandler, None, 'sokhanran_loading'),
    (r'/admin/sokhanran/auto_load', Sokhanran_AutoLoadHandler, None, 'sokhanran_auto_complete'),

    (r'/heyat/shaer', ShaerHandler, None, 'shaer_list'),
    (r'/shaer/loading', Shaer_LoadingPageHandler, None, 'shaer_loading'),
    (r'/admin/shaer/auto_load', Shaer_AutoLoadHandler, None, 'shaer_auto_complete'),

    (r'/ws/like', EchoWebSocket, None, 'ws'),

    # (r'/heyat/users/shaer', ShaerHandler, None, 'shaer'),
    (r'/heyat/comment', SendCommentHandler, None, 'send_comment'),

    (r'/heyat/request', HeyatNewRequestHandler, None, 'send_request'),
    (r'/heyat/showmessage', SendMessageHandler, None, 'show_message'),
    (r'/heyat/score', ScoreHeyatHandler, None, 'heyat_score'),
    (r'/heyat/videos/score', ScoreVideoHandler, None, 'video_score'),
    (r'/heyat/users/score', ScoreuserHandler, None, 'user_score'),

    (r'/error/404', Error404Handler, None, '404'),
    # (r'/error/500', Error500Handler, None, '500')
    (r'/download/(.*)', DownloadFileHandler, None, 'download'),
    (r'/captcha', CaptchaHandler, None, 'captcha'),

]