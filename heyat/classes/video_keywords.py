# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
# PROJECT: heyat_tv
#
# DESCRIPTION: ...
#
#
###############################################################################

from base.models.base_model import *


def check_keyword(keyword):
    try:
        key = Keyword().select().where(Keyword.name == keyword).get()
        return key

    except:
        return -1


def add_keyword(keyword):
    key = Keyword.create(name=keyword)

    return key


def add_keyword_to_video(keywords, video_id):
    try:
        keywords.index(',')
        keywords = keywords.split(',')
        for item in keywords:
            if item:
                key = check_keyword(item)
                if key == -1:
                    key = add_keyword(item)

                vid = Video.select().where(Video.id == video_id).get()
                Video_Keyword.create(fk_video=vid, fk_keyword=key)
        return True

    except:  # when user one keyword inserted
        try:
            if keywords:
                key = check_keyword(keywords)
                if key == -1:
                    key = add_keyword(keywords)
                vid = Video.select().where(Video.id == video_id).get()
                Video_Keyword.create(fk_video=vid, fk_keyword=key)
                return True
        except:
            return False
    return True


# ...................................................
def check_artist(artist):
    try:

        my_artist = User().select().where(SQL('CONCAT (fore_name," ",sur_name) = %s', artist)).get()
        print('match')
        return my_artist
    except:
        try:
            print('if')
            my_artist = User.select().where(SQL('CONCAT (fore_name , sur_name) = %s', artist)).get()
            print('match other')
            return my_artist
        except:
            print('r-1')
            return -1


def add_artist(name):
    print('add')
    role = Role.select().where(Role.id == 13).get()
    user = User.create(fore_name=name, sur_name=" ", password="none", email="none", pic="user.png", is_activeed=1,
                       fk_role=role).id
    print('create')
    return user


def add_artist_to_video(artists, video_id):
    try:
        artists.index(',')
        artists = artists.split(',')

        for item in artists:
            if item:
                art = check_artist(item)
                if art == -1:
                    print('if -1')
                    art = add_artist(item)
                    print(art)

                vid = Video.select().where(Video.id == video_id).get().id
                Artist_has_Video.insert(fk_user=art, fk_video=vid).execute()
        return True

    except Exception as a:
        print(a)# when user one keyword inserted
        try:
            if artists:
                art = check_artist(artists)
                if art == -1:
                    art = add_artist(artists)
                vid = Video.select().where(Video.id == video_id).get().id
                Artist_has_Video.insert(fk_user=art, fk_video=vid).execute()
                return True
        except:
            return False
    return True


# .....................................................

def add_subject_domain_to_video(subjects, video):
    counter = 0  # number of Sublect Domains
    try:
        subjects.index(',')
        subjects = subjects.split(',')

        for item in subjects:
            if item:
                print(item)
                subject = Subject_Domain.select().where(Subject_Domain.name == item).get().id
                print(subject.id)
                SubjectDomain_has_Video.insert(fk_subject_domain=subject, fk_video=video).execute()
                counter = counter + 1
        return counter

    except:  # when user one subject inserted
        try:
            subject = Subject_Domain.select().where(Subject_Domain.name == subjects).get().id
            SubjectDomain_has_Video.insert(fk_subject_domain=subject, Score_has_Video=video).execute()
            counter = counter + 1
            return counter
        except:
            return False




