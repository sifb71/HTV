# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################
import tornado.web
from base.handlers.base import WebBaseHandler
from base.models.base_model import *
from pycket.session import SessionManager


class ChannelsHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        name = self.get_argument('heyatname', "")
        ostan = self.get_argument('ostan', "")
        city = self.get_argument('city', "")

        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'

        addresses = Address().select().where(Address.province ** ("%" + ostan + "%"),
                                             Address.city ** ("%" + city + "%"))
        heyats = Heyat().select().where(Heyat.fk_address << addresses, Heyat.is_active == 1,
                                        Heyat.name ** ("%" + name + "%")).order_by(Heyat.name) \
            .paginate(1,12)
        count = Heyat().select().where(Heyat.fk_address << addresses, Heyat.is_active == 1,
                                       Heyat.name ** ("%" + name + "%")).count()
        self.render('channels.html', login_user=login_user, title='کانال ها', heyats=heyats, page=1, count=count)

    def post(self, *args, **kwargs):
        pass


class HeyatChannelHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        heyat = args[0]
        _id = heyat.split('m', 1)
        session = SessionManager(self)
        login_id = session.get('id')
        heyat = Heyat.select().where(Heyat.id == _id[0]).get()
        user = ""
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
            user = User.select().where(User.id == login_id).get()
            regu = Heyat_has_user.select().where(Heyat_has_user.fk_user == user, Heyat_has_user.fk_heyat == heyat)
            if regu.count():
                request = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat,
                                                        Heyat_has_user.fk_user == user).get().is_accept
            else:
                request = 2

        else:
            login_user = 'مهمان'
            request = -1
        if not user == "" and heyat.fk_admin == user:
            request = 3
        print(request)
        page = int(self.get_argument('page', 1))
        if page < 0:
            page = 1
        videos = Video.select().where(Video.fk_heyat == heyat, Video.status == 3).order_by(
            Video.inserted_date.desc()).paginate(page, 10)
        users = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat, Heyat_has_user.is_accept == 1).order_by(
            Heyat_has_user.fk_user)
        try:
            score = int(Score_has_Heyat.select().where(Score_has_Heyat.fk_user == user,
                                                       Score_has_Heyat.fk_heyat == heyat).get().score / 0.6)
        except:
            score = 0

        count = Video.select().where(Video.fk_heyat == heyat, Video.status == 3).count()

        self.render('heyat-channel.html', login_user=login_user, title='کانال هیئت', heyat=heyat, videos=videos,
                    users=users, request=request, score=score, count=count, page=page)

    def post(self, *args, **kwargs):
        pass


class HeyatVideoHandler(WebBaseHandler):
    def data_received(self, chunk):
        print("data_received")

    def get(self, *args, **kwargs):
        video_name = args[0]
        title = video_name.split('m', 1)
        video_id = title[0]
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
            user = User.select().where(User.id == login_id).get()
        else:
            login_user = 'مهمان'
        video = Video.select().where(Video.id == video_id).get()
        like = Video_Like.select().where(Video_Like.fk_video == video).count()
        artists = Artist_has_Video.select().where(Artist_has_Video.fk_video == video)
        subjects = SubjectDomain_has_Video.select().where(SubjectDomain_has_Video.fk_video == video)
        keywords = Video_Keyword.select().where(Video_Keyword.fk_video == video)
        comments = Comment.select().where(Comment.fk_video == video, Comment.status == 1).order_by(
            Comment.inserted_date.desc())

        try:
            score = int(User_Score_has_Video.select().where(User_Score_has_Video.fk_user == user,
                                                            User_Score_has_Video.fk_video == video).get().score / 0.6)
        except:
            score = 0
        self.render('heyat-video.html', login_user=login_user, title=title[1].replace('_', ' '), video=video,
                    artists=artists, subjects=subjects, keywords=keywords, like=like, login_id=login_id,
                    comments=comments, score=score)

    def post(self, *args, **kwargs):
        pass


class Channel_LoadingHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        pass;

    def post(self, *args, **kwargs):
        print('loading')
        page = self.get_argument('page', "")
        name = self.get_argument('name', "")
        city = self.get_argument('city', "")
        ostan = self.get_argument('ostan', "")
        print('page:', page)

        addresses = Address().select().where(Address.province ** ("%" + ostan + "%"),
                                             Address.city ** ("%" + city + "%"))
        heyats = Heyat().select().where(Heyat.fk_address << addresses, Heyat.is_active == 1,
                                        Heyat.name ** ("%" + name + "%")) \
            .paginate(int(page), 12)

        resualt = {}
        dict__ = {}
        dict__['count'] = heyats.count()

        count = Heyat.select().where(Heyat.is_active == 1).count()

        if int(page) <= count:
            print('id')
            resualt[0] = dict__
            i = 1
            for item in heyats:
                dict__ = {}
                dict__['id'] = item.id
                dict__['name'] = item.name
                dict__['logo'] = item.logo
                dict__['province'] = item.fk_address.province
                dict__['city'] = item.fk_address.city
                dict__['village'] = item.fk_address.village
                resualt[i] = dict__
                i = i + 1
        else:
            resualt = 'end'

        self.write(resualt)


class Heyat_AoutoLoadingHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        pass

    def post(self, *args, **kwargs):
        print('heyatloadding')
        heyat = self.get_argument('heyat', "")
        heyats = Heyat().select().where(Heyat.name ** ("%" + heyat + "%")) \
            .order_by(Heyat.name) \
            .limit(10)
        array = []
        for item in heyats:
            array.append(item.name)

        dict__ = {
            "key": array
        }

        self.write(dict__)
