# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os
from base.handlers.base import WebBaseHandler


class DownloadFileHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        file = args[0]
        title = file.split('m', 1)
        file_name = title[1]
        _file_dir = "static/upload/video/" + title[0]
        _file_path = "%s/%s" % (_file_dir, file_name)
        if not file_name or not os.path.exists(_file_path):
            print(404)
        self.set_header('Content-Type', 'video/mp4')
        self.set_header('Content-Disposition', 'attachment; filename=' + file_name + '')
        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                print(404)
        print(500)
