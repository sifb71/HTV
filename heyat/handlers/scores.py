# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class ScoreHeyatHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print("WWWW")
        heyat_id = self.get_argument('hid')
        score = int(self.get_argument('score')) * 0.6
        if score > 3:
            score = 3
        session = SessionManager(self)
        login_id = session.get('id')
        user = User.select().where(User.id == int(login_id)).get()
        heyat = Heyat.select().where(Heyat.id == int(heyat_id)).get()
        if not Score_has_Heyat.select().where(Score_has_Heyat.fk_user == user,
                                              Score_has_Heyat.fk_heyat == heyat).count():
            Score_has_Heyat.create(fk_heyat=heyat, fk_user=user, score=score)
            oldscore = heyat.score
            Heyat.update(score=oldscore + score).where(Heyat.id == int(heyat_id)).execute()
        else:
            olduserscore = Score_has_Heyat.select().where(Score_has_Heyat.fk_user == user,
                                                          Score_has_Heyat.fk_heyat == heyat).get().score
            print(olduserscore)
            oldscore = heyat.score - olduserscore
            Score_has_Heyat.update(score=score).where(Score_has_Heyat.fk_user == user,
                                                      Score_has_Heyat.fk_heyat == heyat).execute()

            Heyat.update(score=oldscore + score).where(Heyat.id == int(heyat_id)).execute()
        link = str(heyat.id) + "m"
        self.redirect('channel' + '/' + link)
        # self.render(self.redirect("))

    def post(self, *args, **kwargs):
        print('xxxxxx')


class ScoreVideoHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        video_id = self.get_argument('vid')
        score = int(self.get_argument('score')) * 0.6
        if score > 3:
            score = 3
        session = SessionManager(self)
        login_id = session.get('id')
        user = User.select().where(User.id == int(login_id)).get()
        video = Video.select().where(Video.id == int(video_id)).get()
        if not User_Score_has_Video.select().where(User_Score_has_Video.fk_user == user,
                                                   User_Score_has_Video.fk_video == video).count():
            User_Score_has_Video.create(fk_video=video, fk_user=user, score=score)
            oldscore = video.score
            Video.update(score=oldscore + score).where(Video.id == int(video_id)).execute()
        else:
            olduserscore = User_Score_has_Video.select().where(User_Score_has_Video.fk_user == user,
                                                               User_Score_has_Video.fk_video == video).get().score
            print(olduserscore)
            oldscore = video.score - olduserscore
            User_Score_has_Video.update(score=score).where(User_Score_has_Video.fk_user == user,
                                                           User_Score_has_Video.fk_video == video).execute()

            Video.update(score=oldscore + score).where(Video.id == int(video_id)).execute()
        link = str(video.id) + "m"
        self.redirect('../' + 'video' + '/' + link)
        # self.render(self.redirect("))

    def post(self, *args, **kwargs):
        print('xxxxxx')


class ScoreuserHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        user_id = self.get_argument('uid')
        score = int(self.get_argument('score')) * 0.6
        if score > 3:
            score = 3
        session = SessionManager(self)
        login_id = session.get('id')
        adder = User.select().where(User.id == int(login_id)).get()
        _user = User.select().where(User.id == int(user_id)).get()
        if not Score_has_User.select().where(Score_has_User.fk_user == _user,
                                             Score_has_User.fk_user_adder == adder).count():
            Score_has_User.create(fk_user_adder=adder, fk_user=_user, score=score)
            oldscore = _user.score
            User.update(score=oldscore + score).where(User.id == int(user_id)).execute()
        else:
            olduserscore = Score_has_User.select().where(Score_has_User.fk_user == _user,
                                                         Score_has_User.fk_user_adder == adder).get().score
            print(olduserscore)
            oldscore = _user.score - olduserscore
            Score_has_User.update(score=score).where(Score_has_User.fk_user == _user,
                                                     Score_has_User.fk_user_adder == adder).execute()

            User.update(score=oldscore + score).where(User.id == int(user_id)).execute()
        link = str(_user.id) + "m"
        self.redirect('../' + 'user' + '/' + link)
        # self.render(self.redirect("))

    def post(self, *args, **kwargs):
        print('xxxxxx')
