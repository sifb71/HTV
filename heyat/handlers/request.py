# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class HeyatNewRequestHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass

    def post(self, *args, **kwargs):
        _id = self.get_argument('id')
        try:
            session = SessionManager(self)
            login_id = session.get('id')
            if login_id:
                login_user = session.get('fname')
                login_user += " "
                login_user += session.get("lname")
            else:
                login_user = 'مهمان'
            heyat = Heyat.select().where(Heyat.id == _id).get()
            user = User.select().where(User.id == session.get('id')).get()
            if not Heyat_has_user.select().where(Heyat_has_user.fk_user == user,
                                                 Heyat_has_user.fk_heyat == heyat).count():
                Heyat_has_user.create(fk_heyat=heyat, fk_user=user, is_accept=0)

                text = "درخواست عضویت شما در هیئت" + " " + heyat.name + " " + "با موفقیت ارسال شد"
                print("qqqq")
                send_notification(self, heyat.fk_admin, user, "درخواست عضویت در هیئت")
                print("wwww")
            link = "heyat/channel/" + str(heyat.id) + "m" + heyat.name
            print("eee")
            self.render('show_message.html', login_user=login_user, title='پیام', text1=text,
                        text2="پس از تایید مدیر شما عضو هیئت خواهید شد.", link=link.replace(' ', '_'))
            self.write(text)
        except:
            self.write('one error occure!')


class SendMessageHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'
        self.render('show_message.html', login_user=login_user)

    def post(self, *args, **kwargs):
        pass
