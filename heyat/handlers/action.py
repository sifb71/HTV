# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler
from base.models.base_model import *
from pycket.session import SessionManager
from datetime import datetime


class ActionBaseHandler(WebBaseHandler):
    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super(ActionBaseHandler, self).__init__(application, request, **kwargs)

    def on_finish(self):
        super(ActionBaseHandler, self).on_finish()


class SendCommentHandler(ActionBaseHandler):
    def data_received(self, chunk):
        print("data_received")

    def get(self, *args, **kwargs):
        print("jequery")
        code = float(self.get_argument('imgcode'))
        image_str = int(self.get_argument('imgstr'))
        true_code = int(code * 9999)
        print(true_code)
        print(image_str)

        if image_str == true_code:
            print("true")
            self.write('ok')
        else:
            print("false")
            self.write('error')


    def post(self, *args, **kwargs):
        text = self.get_argument('comment')
        video_id = self.get_argument('video')
        imgstr = self.get_argument('imgstr')
        imgcode = self.get_argument('imgcode')

        video = Video.select().where(Video.id == int(video_id)).get()
        session = SessionManager(self)
        login_id = session.get('id')
        user = User.select().where(User.id == login_id)
        false = 0
        if text:
            Comment.create(fk_video=video, fk_user=user, inserted_date=datetime.now(), status=0, text=text)
            text1 = 'پیام شما با موفقیت ارسال شد.'
            text2 = 'پس از تایید مدیر هیئت نمایش داده خواهد شد.'
        else:
            text1 = "لطفا متن پیام را وارد کنید!"
            text2 = ""
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'
        link = "heyat/video/" + str(video.id) + "m" + video.name
        self.render('show_message.html', login_user=login_user, title='پیام', text1=text1,
                    text2=text2, link=link.replace(' ', '_'))
