# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler
from base.models.base_model import *
from pycket.session import SessionManager


class UsersBaseHandler(WebBaseHandler):
    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super(UsersBaseHandler, self).__init__(application, request, **kwargs)

    def on_finish(self):
        super(UsersBaseHandler, self).on_finish()


class ProfileHandler(UsersBaseHandler):
    def data_received(self, chunk):
        print("data_received")

    def get(self, *args, **kwargs):
        profile = args[0]
        title = profile.split('m', 1)
        profile_id = title[0]
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'
        profile = User.select().where(User.id == profile_id).get()

        videos = Artist_has_Video.select().join(Video).where(Artist_has_Video.fk_user == profile,
                                                             Video.status == 3)
        try:
            adder = User.select().where(User.id == int(login_id)).get()
            score = int(Score_has_User.select().where(Score_has_User.fk_user == profile,
                                                      Score_has_User.fk_user_adder == adder).get().score / 0.6)
        except:
            score = 0
        self.render('profile.html', login_user=login_user, title=title[1].replace('_', ' '), profile=profile,
                    videos=videos, score=score)

    def post(self, *args, **kwargs):
        pass


class MaddahHandler(UsersBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'

        role = Role.select().where((Role.id == 7) | (Role.id == 13))

        users = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).paginate(1,
                                                                                                                     12) \
            .order_by(User.sur_name.desc())
        count = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).count()
        self.render('maddah_list.html', login_user=login_user, title='فهرست مداحان', users=users, count=count, page=1)

    def post(self, *args, **kwargs):
        pass


class Maddah_AutoLoadHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        pass;

    def post(self, *args, **kwargs):
        artist = self.get_argument('artist')
        role = Role().select().where((Role.id == 7) | (Role.id == 13))
        artists = User().select().where(User.fk_role << role,
                                        User.fore_name.concat(User.sur_name) ** ("%" + artist + "%")) \
            .limit(10).order_by(User.sur_name.desc())

        array = []
        for item in artists:
            array.append(item.fore_name + ' ' + item.sur_name)

        dict__ = {
            "key": array
        }

        self.write(dict__)


class Maddah_LoadingPageHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        pass;

    def post(self, *args, **kwargs):
        print('maddahloading')
        page = self.get_argument('page', "")
        name = self.get_argument('name', "").strip()
        role = Role().select().where((Role.id == 7) | (Role.id == 13))
        print(name)

        artists = User().select().where(User.fk_role << role,
                                        SQL('(CONCAT (fore_name," ",sur_name) )LIKE  %s', "%" + name + "%")) \
            .paginate(int(page), 12).order_by(User.sur_name.desc())

        resualt = {}
        dict__ = {}
        dict__['count'] = artists.count()

        count = User().select().where(User.fk_role << role,
                                      SQL('(CONCAT (fore_name," ",sur_name)) LIKE  %s', "%" + name + "%")).count()
        print(page)
        print(count)
        if int(page) <= count:
            print('id')
            resualt[0] = dict__
            i = 1
            for item in artists:
                dict__ = {}
                dict__['id'] = item.id
                dict__['name'] = item.fore_name + " " + item.sur_name
                dict__['pic'] = item.pic
                dict__['active'] = item.is_active

                resualt[i] = dict__
                i = i + 1
        else:
            resualt = 'end'

        self.write(resualt)


class SokhanranHandler(UsersBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'

        role = Role.select().where((Role.id == 8) | (Role.id == 13))

        users = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).paginate(1,
                                                                                                                     12).order_by(
            User.sur_name.desc())
        count = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).count()
        self.render('sokhanran_list.html', login_user=login_user, title='فهرست سخنرانان', users=users, count=count,
                    page=1)

    def post(self, *args, **kwargs):
        pass


class Sokhanran_AutoLoadHandler(UsersBaseHandler):
    def get(self, *args, **kwargs):
        pass;

    def post(self, *args, **kwargs):
        artist = self.get_argument('artist')
        role = Role().select().where((Role.id == 8) | (Role.id == 13))
        artists = User().select().where(User.fk_role << role,
                                        User.fore_name.concat(User.sur_name) ** ("%" + artist + "%")) \
            .limit(10)

        array = []
        for item in artists:
            array.append(item.fore_name + ' ' + item.sur_name)

        dict__ = {
            "key": array
        }

        self.write(dict__)


class Sokhanran_LoadingPageHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        pass;

    def post(self, *args, **kwargs):
        page = self.get_argument('page', "")
        name = self.get_argument('name', "").strip()
        role = Role().select().where((Role.id == 8) | (Role.id == 13))
        print(name)

        artists = User().select().where(User.fk_role << role,
                                        SQL('(CONCAT (fore_name," ",sur_name) )LIKE  %s', "%" + name + "%")) \
            .paginate(int(page), 12)

        resualt = {}
        dict__ = {}
        dict__['count'] = artists.count()

        count = User().select().where(User.fk_role << role,
                                      SQL('(CONCAT (fore_name," ",sur_name)) LIKE  %s', "%" + name + "%")).count()

        if int(page) <= count:
            print('id')
            resualt[0] = dict__
            i = 1
            for item in artists:
                dict__ = {}
                dict__['id'] = item.id
                dict__['name'] = item.fore_name + " " + item.sur_name
                dict__['pic'] = item.pic
                dict__['active'] = item.is_active

                resualt[i] = dict__
                i = i + 1
        else:
            resualt = 'end'

        self.write(resualt)


class ShaerHandler(UsersBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'

        role = Role.select().where((Role.id == 12) | (Role.id == 13))

        users = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).paginate(1,
                                                                                                                     12).order_by(
            User.sur_name.desc())
        count = User.select().where(User.fk_role << role, (User.is_active == 1) | (User.is_active == None)).count()
        self.render('shaer_list.html', login_user=login_user, title='فهرست شاعران', users=users, count=count, page=1)

    def post(self, *args, **kwargs):
        pass


class Shaer_AutoLoadHandler(UsersBaseHandler):
    def get(self, *args, **kwargs):
        pass;

    def post(self, *args, **kwargs):
        artist = self.get_argument('artist')
        role = Role().select().where((Role.id == 12) | (Role.id == 13))
        artists = User().select().where(User.fk_role << role,
                                        User.fore_name.concat(User.sur_name) ** ("%" + artist + "%")) \
            .limit(10)

        array = []
        for item in artists:
            array.append(item.fore_name + ' ' + item.sur_name)

        dict__ = {
            "key": array
        }

        self.write(dict__)


class Shaer_LoadingPageHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print('get')
        pass;

    def post(self, *args, **kwargs):
        page = self.get_argument('page', "")
        name = self.get_argument('name', "").strip()
        role = Role().select().where((Role.id == 12) | (Role.id == 13))
        print(name)

        artists = User().select().where(User.fk_role << role,
                                        SQL('(CONCAT (fore_name," ",sur_name) )LIKE  %s', "%" + name + "%")) \
            .paginate(int(page), 12)

        resualt = {}
        dict__ = {}
        dict__['count'] = artists.count()

        count = User().select().where(User.fk_role << role,
                                      SQL('(CONCAT (fore_name," ",sur_name)) LIKE  %s', "%" + name + "%")).count()

        if int(page) <= count:
            print('id')
            resualt[0] = dict__
            i = 1
            for item in artists:
                dict__ = {}
                dict__['id'] = item.id
                dict__['name'] = item.fore_name + " " + item.sur_name
                dict__['pic'] = item.pic
                dict__['active'] = item.is_active

                resualt[i] = dict__
                i = i + 1
        else:
            resualt = 'end'

        self.write(resualt)
