# -*- coding: utf-8 -*-
###############################################################################
#
# AUTHOR: iman_feyzbakhsh
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from base.handlers.base import WebBaseHandler

from heyat.classes.video_keywords import *
from pycket.session import SessionManager
# from heyat.handlers.video_upload import create_path



class DashboardHandler(WebBaseHandler):

    def get(self, *args, **kwargs):
        session = SessionManager(self)
        user_id = session.get("id")
        user_name = session.get("fname")
        user_name += " " + session.get("lname")
        admin = User.select().where(User.id == user_id)

        try:
            heyat = Heyat.select().where(Heyat.fk_admin == admin).get()
            videos = Video.select().where(Video.fk_heyat == heyat)
            heyat_has_user = Heyat_has_user().select().where(Heyat_has_user.fk_heyat == heyat ,
                                                             Heyat_has_user.is_active == 0)
            notifications = Notification.select().where(Notification.fk_user == user_id)
            users = Heyat_has_user.select().where(Heyat_has_user.fk_heyat == heyat)

        except:
            heyat = None
        self.render('dashboard.html' , user_name = user_name , heyat = heyat ,videos = videos , member = heyat_has_user , notifications=notifications, users=users)

    def post(self, *args, **kwargs):
        session = SessionManager(self)

        name = self.get_argument('name')
        headtxt = self.get_argument('headtxt')
        main_title = self.get_argument('maintxt')
        comment = self.get_argument('comment')
        type_ = self.get_argument('type')
        video_date = self.get_argument('date')
        keywords = self.get_argument('key_words')

        province = self.get_argument('ostan').strip()
        city = self.get_argument('city').strip()
        village = self.get_argument('village').strip()
        address = self.get_argument('address').strip()

        if(province and city  and address):    #if enter a new address for video add it to address table
            fk_address = Address.create(province = province , city = city , village = village , address = address)

        else:
            fk_address = None

        mem = User.select().where(User.id == session.get('id')).get()
        heyat = Heyat.select().where(Heyat.fk_admin == mem).get()


        if name and main_title and comment and type_ and video_date:
            id_ = Video.create(name=name,headline = headtxt ,headline_main=main_title, comment=comment, type=type_, url= session.get("temp_name"),
                               fk_address = fk_address ,fk_uploader=mem, fk_heyat = heyat, upload_date=video_date).get_id()

            session.delete("temp_name")  #delete video name from session
            try:
                keywords.index(',')
                keywords = keywords.split(',')
                for item in keywords:
                    if item:
                        key = check_keyword(item)
                        print(key)
                        if key == -1:
                            add_keyword(item)
                            key = check_keyword(item)

                        vid = Video.select().where(Video.id == id_).get()
                        add_keyword_to_video(key, vid)

            except:
                if keywords:
                    key = check_keyword(keywords)
                    if key == -1:
                        add_keyword(keywords)
                        key = check_keyword(keywords)
                    vid = Video.select().where(Video.id == id_).get()
                    add_keyword_to_video(key, vid)
        else:
            self.write("Enter All Argument")

class Active_memberHandler(WebBaseHandler):

    def get(self, *args, **kwargs):
        pass

    def post(self, *args, **kwargs):
        print('post')
        _id = self.get_argument('id')
        Heyat_has_user.update(is_active = 1).where(Heyat_has_user.id == _id).execute()
        self.redirect(self.reverse_url("dashboard"))


class Delete_memberHandler(WebBaseHandler):

    def get(self, *args, **kwargs):
        pass

    def post(self, *args, **kwargs):
        print('post')
        _id = self.get_argument('id')
        Heyat_has_user.delete().where(Heyat_has_user.id == _id).execute()
        self.redirect(self.reverse_url("dashboard"))


