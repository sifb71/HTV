# -*- coding: utf-8 -*-
###############################################################################
#
#    AUTHOR: mohammad_hefazi
#    PROJECT: heyat_tv
#
#    DESCRIPTION: ...
#
#
###############################################################################

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from administration.classes.Public_actions import *
from base.handlers.base import WebBaseHandler


class Error404Handler(WebBaseHandler):
    def get(self, *args, **kwargs):
        session = SessionManager(self)
        login_id = session.get('id')
        if login_id:
            login_user = session.get('fname')
            login_user += " "
            login_user += session.get("lname")
        else:
            login_user = 'مهمان'
        self.render('404.html', login_user=login_user, title='خطا')

    def post(self, *args, **kwargs):
        pass
