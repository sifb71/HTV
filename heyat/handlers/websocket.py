__author__ = 'DreamLove'
import tornado.escape
import tornado.web

from tornado import web, websocket
from tornado.escape import utf8, native_str, to_unicode
from pycket.session import SessionManager
from base.models.base_model import *

clients = []


class EchoWebSocket(websocket.WebSocketHandler):
    def on_message(self, message):
        if message.rfind('+') == -1:
            video = Video.select().where(Video.id == message).get()
            visit = video.visit + 1
            Video.update(visit=visit).where(Video.id == message).execute()
            print(visit)
            for item in clients:
                resualt = str(visit)
                item.write_message(resualt)

        else:
            vidio_id = message.split('+')[1]
            user_id = message.split('+')[2]
            video = Video.select().where(Video.id == vidio_id).get()
            user = User.select().where(User.id == user_id).get()
            print('ok')
            try:
                Video_Like.select().where(Video_Like.fk_video == video, Video_Like.fk_usre == user).get()
                self.write_message('error')
            except:
                Video_Like.create(fk_usre=user, fk_video=video)
                score = video.score + 2
                Video.update(score=score).where(Video.id == video.id).execute()
                count = Video_Like.select().where(Video_Like.fk_video == video).count()
                for item in clients:
                    resualt = str(count) + '+' + str(score)
                    item.write_message(resualt)

    def data_received(self, chunk):
        pass

    def open(self):
        print("WebSocket opened")
        if self not in clients:
            clients.append(self)

    def on_close(self):
        print("WebSocket closed")
        if self in clients:
            print("removed")
            clients.remove(self)

    def send_error(self, *args, **kwargs):
        print("error")

    def write_message(self, message, binary=False):
        print(message)
        if self.ws_connection is None:
            print('error')
        if isinstance(message, dict):
            message = tornado.escape.json_encode(message)
        self.ws_connection.write_message(message, binary=binary)


class ApiHandler(websocket.WebSocketHandler):
    pass
