# # -*- coding: utf-8 -*-
# ###############################################################################
# #
# #    AUTHOR: mohammad_hefazi
# #    PROJECT: heyat_tv
# #
# #    DESCRIPTION: ...
# #
# #
# ###############################################################################
#
import tornado.web
import sys
from base.handlers.base import WebBaseHandler

if not hasattr(sys, 'pypy_version_info'):
    from captcha.image import ImageCaptcha, WheezyCaptcha
from io import BytesIO
from heyat.classes.image import ImageCaptcha

from heyat.classes.test import *


class CaptchaHandler(WebBaseHandler):
    def get(self, *args, **kwargs):
        print("create")
        # audio = AudioCaptcha()
        image = ImageCaptcha()
        _rand = self.get_argument('_xrn')
        print(_rand)
        rand = str(int(float(_rand) * 9999))
        # rand = str(random.randrange(1000, 9999, 4))

        # data = audio.generate(rand)
        # print("WWW")
        # assert isinstance(data, bytearray)
        # file = 'static/captcha/create/' + rand + '.wav'
        # audio.write(rand, file)

        data = image.generate(rand)
        assert isinstance(data, BytesIO)
        file = 'static/captcha/create/' + rand + '.png'
        image.write(rand, file)

        file_name = rand + '.png'
        _file_dir = "static/captcha/create/"
        _file_path = "%s/%s" % (_file_dir, file_name)
        if not file_name or not os.path.exists(_file_path):
            print(404)
        self.set_header("Content-Type", "image/png")
        self.set_header('Content-Disposition', 'attachment; filename=' + file_name + '')
        with open(_file_path, "rb") as f:
            try:
                url = ''
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)

                    else:
                        f.close()
                        self.finish()
                        return
            except:
                print(404)
        print(500)
        self.write('<img style="-webkit-user-select: none" src="' '">')
